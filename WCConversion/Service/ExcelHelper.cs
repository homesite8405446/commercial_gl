﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class ExcelHelper
    {
        /// <summary>
        /// Load sheet data into a data table by sheet name.
        /// </summary>
        /// <param name="fileName">Input Excel file name</param>
        /// <param name="sheetName">Sheet name which will be loaded</param>
        /// <returns></returns>
        public DataTable LoadWorksheetInDataTable(string fileName, string hasHeader, string sheetName = "")
        {
            FileInfo file = new FileInfo(fileName);
            DataTable sheetData = new DataTable();
            using (OleDbConnection conn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + file.DirectoryName + ";Extended Properties='Text;HDR=" + hasHeader + ";FMT=Delimited(,);'"))
            {
                using (OleDbCommand cmd = new OleDbCommand(string.Format("SELECT * FROM [{0}]", file.Name), conn))
                {
                    conn.Open();
                    using (OleDbDataAdapter adp = new OleDbDataAdapter(cmd))
                    {
                        adp.Fill(sheetData);
                    }
                }
            }
            return sheetData;
        }

        /// <summary>
        /// Conver csv file to DataTable
        /// </summary>
        /// <param name="strFilePath"></param>
        /// <returns></returns>
        public DataTable ConvertCSVtoDataTable(string strFilePath)
        {
            DataTable dt = new DataTable();
            using (StreamReader sr = new StreamReader(strFilePath))
            {
                string[] headers = sr.ReadLine().Split(',');
                foreach (string header in headers)
                {
                    dt.Columns.Add(header);
                }
                while (!sr.EndOfStream)
                {
                    string[] rows = sr.ReadLine().Split(',');
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = rows[i];
                    }
                    dt.Rows.Add(dr);
                }

            }
            return dt;
        }

        /// <summary>
        /// Get Full Name of the State by State Code
        /// </summary>
        /// <param name="stateTemplateFullPath"></param>
        /// <param name="stateCode"></param>
        /// <returns></returns>
        public IStateInfo GetStateInfo(string stateTemplateFullPath, string stateCode)
        {
            ExcelHelper xlHelper = new ExcelHelper();
            var stateInfo = new StateInfo();

            if (File.Exists(stateTemplateFullPath))
            {
                var dtState = xlHelper.ConvertCSVtoDataTable(stateTemplateFullPath);
                for (int i = 0; i < dtState.Rows.Count; i++)
                {
                    if (Convert.ToString(dtState.Rows[i]["StateCode"]).Trim().ToUpper() == stateCode.Trim().ToUpper())
                    {
                        stateInfo.LongState = Convert.ToString(dtState.Rows[i]["CL"]).Trim();
                        stateInfo.StateType = Convert.ToString(dtState.Rows[i]["StateType"]).Trim();
                        stateInfo.IsFound = true;
                    }
                }
            }

            return stateInfo;
        }

    }
}
