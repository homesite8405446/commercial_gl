﻿using System.Net.Mail;
using System.Net.Mime;
using System.Text;

namespace WCConversion
{
    public class EmailService
    {
        /// <summary>
        /// Sends Email
        /// </summary>
        /// <param name="hostName"></param>
        /// <param name="portNo"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="viewBody"></param>
        /// <param name="cc"></param>
        /// <returns></returns>
        public static bool SendEmail(string hostName, string portNo, string from, string to, string subject, AlternateView viewBody, string cc = "")
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress(from);
            foreach (var toaddress in to.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
            {
                message.To.Add(new MailAddress(toaddress.Trim()));
            }
            if (!string.IsNullOrEmpty(cc))
            {
                foreach (var ccaddress in cc.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    message.CC.Add(new MailAddress(ccaddress.Trim()));
                }
            }
            message.Subject = subject;
            message.AlternateViews.Add(viewBody);
            message.BodyEncoding = Encoding.UTF8;
            message.IsBodyHtml = true;
            
            string host = hostName;
            int port = Convert.ToInt32(portNo);
            SmtpClient client = new SmtpClient(host, port);
            client.EnableSsl = true;
            client.UseDefaultCredentials = true;
            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Sends email along with attachment
        /// </summary>
        /// <param name="hostName"></param>
        /// <param name="portNo"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="viewBody"></param>
        /// <param name="attachments"></param>
        /// <param name="cc"></param>
        /// <returns></returns>
        public static bool SendEmail(string hostName, string portNo, string from, string to, string subject, AlternateView viewBody, IList<string> attachments, string cc = "")
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress(from);
            foreach (var toaddress in to.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
            {
                message.To.Add(new MailAddress(toaddress.Trim()));
            }
            if (!string.IsNullOrEmpty(cc))
            {
                foreach (var ccaddress in cc.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    message.CC.Add(new MailAddress(ccaddress.Trim()));
                }
            }
            message.Subject = subject;
            message.AlternateViews.Add(viewBody);
            message.BodyEncoding = Encoding.UTF8;
            message.IsBodyHtml = true;

            Attachment attachment;
            foreach (string item in attachments)
            {
                attachment = new Attachment(item);
                message.Attachments.Add(attachment);
            }

            string host = hostName;
            int port = Convert.ToInt32(portNo);
            SmtpClient client = new SmtpClient(host, port);
            client.EnableSsl = true;
            client.UseDefaultCredentials = true;
            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Generate html mail body from a simple string
        /// </summary>
        /// <param name="mailBody"></param>
        /// <returns></returns>
        public static AlternateView GenerateHtmlMailBody(string mailBody)
        {
            string str = string.IsNullOrEmpty(mailBody) ? "" : @"  
            <table>  
                <tr>  
                    <td> " + mailBody + @"  
                    </td>  
                </tr>  
            ";
            AlternateView AV = AlternateView.CreateAlternateViewFromString(str, null, MediaTypeNames.Text.Html);
            return AV;
        }

        /// <summary>
        /// Sends Start Email
        /// </summary>
        /// <param name="emailConfig"></param>
        /// <param name="startEmail"></param>
        /// <param name="machineName"></param>
        /// <param name="strStartDateTime"></param>
        /// <returns></returns>
        public static bool SendStartEmail(EmailConfig emailConfig, EmailInfo startEmail, string machineName, string strStartDateTime)
        {
            string subject = startEmail.Subject.Replace("%Machinename%", machineName);
            string strHtml = File.ReadAllText(startEmail.BodyTemplate);
            strHtml = strHtml.Replace("%strStartDateTime%", strStartDateTime);
            AlternateView viewBody = AlternateView.CreateAlternateViewFromString(strHtml, null, MediaTypeNames.Text.Html);
            return SendEmail(emailConfig.HostName, emailConfig.PortNo, emailConfig.ReportFromEmail, emailConfig.OpsTeamEmailAddress, subject, viewBody);
        }

        /// <summary>
        /// Send completion email
        /// </summary>
        /// <param name="emailConfig"></param>
        /// <param name="completionEmail"></param>
        /// <param name="machineName"></param>
        /// <param name="strStartDateTime"></param>
        /// <param name="strEndDateTime"></param>
        /// <returns></returns>
        public static bool SendCompletionEmail(EmailConfig emailConfig, EmailInfo completionEmail, string machineName, string strStartDateTime, string strEndDateTime, List<string> attachmentList)
        {
            string subject = completionEmail.Subject.Replace("%Machinename%", machineName);
            string strHtml = File.ReadAllText(completionEmail.BodyTemplate);
            strHtml = strHtml.Replace("%strStartDateTime%", strStartDateTime);
            strHtml = strHtml.Replace("%strEndDateTime%", strEndDateTime);
            AlternateView viewBody = AlternateView.CreateAlternateViewFromString(strHtml, null, MediaTypeNames.Text.Html);
            return SendEmail(emailConfig.HostName, emailConfig.PortNo, emailConfig.ReportFromEmail, emailConfig.OpsTeamEmailAddress, subject, viewBody, attachmentList);
        }

        /// <summary>
        /// Shared drive access error email
        /// </summary>
        /// <param name="emailConfig"></param>
        /// <param name="shareDriveErrorEmail"></param>
        /// <param name="machineName"></param>
        /// <param name="sharedDrivePath"></param>
        /// <returns></returns>
        public static bool SendShareDriveErrorEmail(EmailConfig emailConfig, EmailInfo shareDriveErrorEmail, string machineName, string sharedDrivePath)
        {
            string subject = shareDriveErrorEmail.Subject.Replace("%Machinename%", machineName);
            string strHtml = File.ReadAllText(shareDriveErrorEmail.BodyTemplate);
            strHtml = strHtml.Replace("%SharedDrivePath%", sharedDrivePath);
            AlternateView viewBody = AlternateView.CreateAlternateViewFromString(strHtml, null, MediaTypeNames.Text.Html);
            return SendEmail(emailConfig.HostName, emailConfig.PortNo, emailConfig.ReportFromEmail, emailConfig.OpsTeamEmailAddress, subject, viewBody);
        }

        /// <summary>
        /// Send input file missing email
        /// </summary>
        /// <param name="emailConfig"></param>
        /// <param name="inputFileMissingEmail"></param>
        /// <param name="machineName"></param>
        /// <param name="inputFileName"></param>
        /// <param name="sharedDrivePath"></param>
        /// <returns></returns>
        public static bool SendInputFileMissingEmail(EmailConfig emailConfig, EmailInfo inputFileMissingEmail, string machineName, string inputFileName, string sharedDrivePath)
        {
            string subject = inputFileMissingEmail.Subject.Replace("%Machinename%", machineName);
            string strHtml = File.ReadAllText(inputFileMissingEmail.BodyTemplate);
            strHtml = strHtml.Replace("%InputFileName%", inputFileName);
            strHtml = strHtml.Replace("%InputPath%", sharedDrivePath);
            AlternateView viewBody = AlternateView.CreateAlternateViewFromString(strHtml, null, MediaTypeNames.Text.Html);
            return SendEmail(emailConfig.HostName, emailConfig.PortNo, emailConfig.ReportFromEmail, emailConfig.OpsTeamEmailAddress, subject, viewBody);
        }

        /// <summary>
        /// Send hard stop email(technical exception)
        /// </summary>
        /// <param name="emailConfig"></param>
        /// <param name="hardStopEmail"></param>
        /// <param name="machineName"></param>
        /// <param name="strStartDateTime"></param>
        /// <param name="errMsg"></param>
        /// <param name="stacktrace"></param>
        /// <returns></returns>
        public static bool SendHardStopEmail(EmailConfig emailConfig, EmailInfo hardStopEmail, string machineName, string strStartDateTime, string errMsg, string stacktrace)
        {
            string subject = hardStopEmail.Subject.Replace("%Machinename%", machineName);
            string strHtml = File.ReadAllText(hardStopEmail.BodyTemplate);
            strHtml = strHtml.Replace("%ErrorMessage%", errMsg);
            strHtml = strHtml.Replace("%StackTrace%", stacktrace);
            strHtml = strHtml.Replace("%strStartDateTime%", strStartDateTime);
            AlternateView viewBody = AlternateView.CreateAlternateViewFromString(strHtml, null, MediaTypeNames.Text.Html);
            return SendEmail(emailConfig.HostName, emailConfig.PortNo, emailConfig.ReportFromEmail, emailConfig.OpsTeamEmailAddress, subject, viewBody);
        }

        /// <summary>
        /// Send login failed email
        /// </summary>
        /// <param name="emailConfig"></param>
        /// <param name="loginFailedEmail"></param>
        /// <param name="machineName"></param>
        /// <returns></returns>
        public static bool SendLoginFailedEmail(EmailConfig emailConfig, EmailInfo loginFailedEmail, string machineName, string expMsg)
        {
            string subject = loginFailedEmail.Subject.Replace("%Machinename%", machineName);
            string strHtml = File.ReadAllText(loginFailedEmail.BodyTemplate);
            strHtml = strHtml.Replace("%ExceptionMsg%", expMsg);
            AlternateView viewBody = AlternateView.CreateAlternateViewFromString(strHtml, null, MediaTypeNames.Text.Html);
            return SendEmail(emailConfig.HostName, emailConfig.PortNo, emailConfig.ReportFromEmail, emailConfig.OpsTeamEmailAddress, subject, viewBody);
        }

        /// <summary>
        /// Send Final Report Email
        /// </summary>
        /// <param name="emailConfig"></param>
        /// <param name="reportEmail"></param>
        /// <param name="reportCard"></param>
        /// <param name="machineName"></param>
        /// <param name="strStartDateTime"></param>
        /// <param name="strEndDateTime"></param>
        /// <returns></returns>
        public static bool SendReportEmail(EmailConfig emailConfig, EmailInfo reportEmail, IReportCard reportCard, string machineName, string strStartDateTime, string strEndDateTime)
        {
            string subject = reportEmail.Subject.Replace("%Machinename%", machineName);
            string strHtml = File.ReadAllText(reportEmail.BodyTemplate);
            strHtml = strHtml.Replace("%strStartDateTime%", strStartDateTime);
            strHtml = strHtml.Replace("%strEndDateTime%", strEndDateTime);
            strHtml = strHtml.Replace("%processName%", reportCard.ProcessName);
            strHtml = strHtml.Replace("%totalRecordsCount%", reportCard.TotalFilesCount);
            strHtml = strHtml.Replace("%successRecordsCount%", reportCard.SuccessFilesCount);
            strHtml = strHtml.Replace("%failureRecordsCount%", reportCard.FailureFilesCount);
            AlternateView viewBody = AlternateView.CreateAlternateViewFromString(strHtml, null, MediaTypeNames.Text.Html);
            return SendEmail(emailConfig.HostName, emailConfig.PortNo, emailConfig.ReportFromEmail, emailConfig.ReportToEmail, subject, viewBody, emailConfig.OpsTeamEmailAddress);
        }
    }
}
