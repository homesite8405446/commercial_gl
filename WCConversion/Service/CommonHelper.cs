﻿using Core.Utilities;
using Microsoft.AspNetCore.Components.Forms;
using System.Data;
using System.Diagnostics;
using System.Security.Principal;
using System.Text;
using System.Web;
using WildfireMoratorium;

namespace WCConversion
{
    public class CommonHelper
    {
        public CommonHelper()
        {
        }

        /// <summary>
        /// It checks if directory is accessible or not
        /// </summary>
        /// <param name="dirPath">Folder Path</param>
        /// <param name="throwIfFails">It will throw exception if directory is not accessible, if it is set with true</param>
        /// <returns></returns>
        public bool IsDirectoryWritable(string dirPath, bool throwIfFails = false)
        {
            try
            {
                using (FileStream fs = File.Create(
                    Path.Combine(
                        dirPath,
                        Path.GetRandomFileName()
                    ),
                    1,
                    FileOptions.DeleteOnClose)
                )
                { }
                return true;
            }
            catch
            {
                if (throwIfFails)
                    throw;
                else
                    return false;
            }
        }

        /// <summary>
        /// Used to backup file.
        /// </summary>
        /// <param name="sourceDir">Source directory</param>
        /// <param name="targetDir">Target directory</param>
        /// <param name="fileName">File name with extension</param>
        public void BackupFile(string sourceDir, string targetDir, string fileName)
        {
            try
            {
                if (File.Exists(sourceDir + "\\" + fileName))
                {
                    if (!Directory.Exists(targetDir))
                    {
                        Directory.CreateDirectory(targetDir);
                    }
                    File.Copy(sourceDir + "\\" + fileName, targetDir + "\\" + fileName);
                }
            }
            catch (Exception) { }
        }

        /// <summary>
        /// Used to delete file
        /// </summary>
        /// <param name="fileDirectory">File path</param>
        /// <param name="fileName">File name with extension</param>
        public void DeleteFile(string fileDirectory, string fileName)
        {
            try
            {
                if (File.Exists(fileDirectory + "\\" + fileName))
                {
                    File.Delete(fileDirectory + "\\" + fileName);
                }
            }
            catch (Exception) { }
        }

        /// <summary>
        /// Delete all files and folders from the directory
        /// </summary>
        /// <param name="fileDirectory"></param>
        public void DeleteAllFile(string fileDirectory)
        {
            try
            {
                if (Directory.Exists(fileDirectory))
                {
                    DirectoryInfo di = new DirectoryInfo(fileDirectory);
                    foreach (FileInfo file in di.GetFiles())
                    {
                        file.Delete();
                    }
                    foreach (DirectoryInfo dir in di.GetDirectories())
                    {
                        dir.Delete(true);
                    }
                }
            }
            catch (Exception) { }
        }

        /// <summary>
        /// Move file from source directory to destination directory
        /// </summary>
        /// <param name="sourceDir"></param>
        /// <param name="targetDir"></param>
        /// <param name="fileName"></param>
        public void MoveFile(string sourceDir, string targetDir, string fileName)
        {
            try
            {
                if (File.Exists(sourceDir + "\\" + fileName))
                {
                    File.Move(sourceDir + "\\" + fileName, targetDir + "\\" + fileName, true);
                }
            }
            catch (Exception) { }
        }

        /// <summary>
        /// Checks file status into a folder
        /// </summary>
        /// <param name="fileDirectory">File directory</param>
        /// <param name="fileName">File name with extension</param>
        /// <returns></returns>
        public bool GetFileStatus(string fileDirectory, string fileName)
        {
            bool fileStatus = false;

            Stopwatch sw = new Stopwatch();
            sw.Start();
            while (true)
            {
                Console.WriteLine(fileDirectory);
                DirectoryInfo fileRoot = new DirectoryInfo(fileDirectory);
                FileInfo[] listfiles = fileRoot.GetFiles(fileName);
                if (listfiles.Length > 0)
                {
                    fileStatus = true;
                    break;
                }

                if (sw.ElapsedMilliseconds > 3000)
                    break;  //breaking after 3 secs if there is no excel file
            }
            sw.Stop();

            return fileStatus;
        }

        /// <summary>
        /// Return list of file from a directory
        /// </summary>
        /// <param name="fileDirectory">Folder Path</param>
        /// <param name="fileFormat">Example: *.xml</param>
        /// <returns></returns>
        public List<FileInfo> GetFileList(string fileDirectory, string fileFormat)
        {
            DirectoryInfo fileRoot = new DirectoryInfo(fileDirectory);
            FileInfo[] listfiles = fileRoot.GetFiles(fileFormat);

            return listfiles.ToList();
        }

        /// <summary>
        /// Get latest modified date of file
        /// </summary>
        /// <param name="fileDirectory">File directory</param>
        /// <param name="fileName">File name with extension</param>
        /// <returns></returns>
        public DateTime GetLatestModifiedDate(string fileDirectory, string fileName)
        {
            DateTime latestDate = DateTime.MinValue;
            DirectoryInfo fileRoot = new DirectoryInfo(fileDirectory);
            FileInfo[] fileInfoList = fileRoot.GetFiles(fileName);

            if (fileInfoList.Count() > 0)
            {
                var fileInfo = fileInfoList.First();
                latestDate = fileInfo.LastWriteTime;
            }

            return latestDate;
        }

        /// <summary>
        /// Retrieve string content from any txt/html file
        /// </summary>
        /// <param name="file">file full path along with extension</param>
        /// <returns></returns>
        public string GetText(string file)
        {
            string text = string.Empty;
            if (file == null)
            {
                throw new ArgumentNullException("html");
            }

            if (File.Exists(file))
            {
                text = File.ReadAllText(file);
            }
            else
            {
                throw new Exception("Unable to read file");
            }

            return text;
        }

        /// <summary>
        /// Convert data table to html table without any styling
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public string ConvertDataTableToHTML(DataTable dt)
        {
            string html = "<table>";
            //add header row
            html += "<th>";
            for (int i = 0; i < dt.Columns.Count; i++)
                html += "<td>" + dt.Columns[i].ColumnName + "</td>";
            html += "</th>";
            //add rows
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                html += "<tr>";
                for (int j = 0; j < dt.Columns.Count; j++)
                    html += "<td>" + dt.Rows[i][j].ToString() + "</td>";
                html += "</tr>";
            }
            html += "</table>";
            return html;
        }

        /// <summary>
        /// Decrypt the encrypted text
        /// </summary>
        /// <param name="encryptedKeyFilePath"></param>
        /// <returns></returns>
        public string GetSecretKey(string encryptedKeyFilePath)
        {
            string userId = WindowsIdentity.GetCurrent().Name.Replace(@"CAMELOT\", "");
            try
            {
                Password pw = new Password(userId);
                return pw.Decrypt("", encryptedKeyFilePath, "");
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get Capping Status
        /// </summary>
        /// <param name="payPlanPath">Pay Plan Full Path in .csv Format</param>
        /// <param name="stateCode"></param>
        /// <param name="premium"></param>
        /// <param name="oldPremium"></param>
        /// <returns></returns>
        public bool GetCappingStatus(string payPlanPath, string stateCode, double premium, double oldPremium)
        {
            bool status = false;
            ExcelHelper excelHelper = new ExcelHelper();
            try
            {
                DataTable dtPayPlan = excelHelper.ConvertCSVtoDataTable(payPlanPath);
                List<PayPlan> payPlans = new List<PayPlan>();

                foreach (DataRow dr in dtPayPlan.Rows)
                {
                    PayPlan payPlan = new PayPlan();
                    payPlan.State = Convert.ToString(dr[0]);
                    payPlan.MinAdjustedPayPlan = Convert.ToDouble(dr[1]);
                    payPlan.MaxAdjustedPayPlan = Convert.ToDouble(dr[2]);
                    payPlans.Add(payPlan);
                }

                if (oldPremium != 0)
                {
                    double premiumDiff = premium - oldPremium;
                    double premiumDiffInPercentage = Math.Round((premiumDiff / oldPremium) * 100, 2);
                    var payPlan = payPlans.Where(p => p.State.Trim().ToUpper() == stateCode.Trim().ToUpper()).FirstOrDefault();
                    if (payPlan != null)
                    {
                        if (premiumDiffInPercentage >= payPlan.MinAdjustedPayPlan && premiumDiffInPercentage <= payPlan.MaxAdjustedPayPlan)
                        {
                            status = true;
                        }
                    }
                }
            }
            catch { }

            return status;
        }

        /// <summary>
        /// Generate Output File in xml format
        /// </summary>
        /// <param name="outputPath"></param>
        /// <param name="screenAutomateStatus"></param>
        /// <param name="inputXmlData"></param>
        /// <param name="processedInputFileName"></param>
        /// <param name="umbrellaValidationPath"></param>
        /// <returns></returns>
        public RPA_RESULTS GenerateOutputFile(string outputPath, IScreenAutomateStatus screenAutomateStatus, InputResult inputXmlData, string processedInputFileName, string umbrellaValidationPath)
        {
            RPA_RESULTS rpaResult = new RPA_RESULTS();
            XmlHelper xmlHelper = new XmlHelper();
            try
            {
                if (!Directory.Exists(outputPath))
                {
                    Directory.CreateDirectory(outputPath);
                }

                if(!Directory.Exists(umbrellaValidationPath))
                {
                    Directory.CreateDirectory(umbrellaValidationPath);
                }
                rpaResult.XML_NAME = processedInputFileName;

                if (inputXmlData != null)
                {
                    rpaResult.PRIORPOLICYNUMBER = inputXmlData.DF_CORR_MASTER.POLICYNUMBER;
                    rpaResult.TRANSACTIONGUID = inputXmlData.DF_CORR_MASTER.TRANSACTIONGUID;
                    rpaResult.POLICYTYPE = inputXmlData.DF_CORR_MASTER.POLICYTYPE;
                    if (!string.IsNullOrEmpty(screenAutomateStatus.QuoteNumber))
                    {
                        rpaResult.QUOTENUM = screenAutomateStatus.QuoteNumber;
                    }
                    if (!string.IsNullOrEmpty(screenAutomateStatus.NewPolicyNumber))
                    {
                        rpaResult.POLICYNUM = screenAutomateStatus.NewPolicyNumber;
                    }
                    rpaResult.STATUS = screenAutomateStatus.StatusPartial ? Status.Partial : (screenAutomateStatus.Status ? Status.Success : Status.Failure);
                    if (!string.IsNullOrEmpty(screenAutomateStatus.ErrMsg))
                    {
                        rpaResult.ERRMSG = screenAutomateStatus.ErrMsg;
                    }
                    if (!string.IsNullOrEmpty(screenAutomateStatus.TotalPremium))
                    {
                        rpaResult.TOTAL_PREMIUM = screenAutomateStatus.TotalPremium;
                    }

                    xmlHelper.GenerateOutputResult(rpaResult, outputPath + "\\" + rpaResult.TRANSACTIONGUID + "_Complete.XML");
                    xmlHelper.GenerateOutputResult(rpaResult, umbrellaValidationPath + "\\" + rpaResult.TRANSACTIONGUID + "_Complete.XML");
                }
                else {
                    rpaResult.STATUS = Status.Invalid;
                    rpaResult.ERRMSG = "Invalid Xml. Unable to read it.";
                    xmlHelper.GenerateOutputResult(rpaResult, outputPath + "\\" + processedInputFileName + "_Invalid.XML");
                    xmlHelper.GenerateOutputResult(rpaResult, umbrellaValidationPath + "\\" + processedInputFileName + "_Invalid.XML");
                }

            }
            catch{ }

            return rpaResult;
        }

        /// <summary>
        /// Convert all xml files to a csv file
        /// </summary>
        /// <param name="umbrellaValidationFolder"></param>
        /// <returns></returns>
        public string GenerateOutputCSV(string umbrellaValidationFolder)
        {
            XmlHelper xmlHelper = new XmlHelper();
            var csv = new StringBuilder();
            string executionReportFolder = umbrellaValidationFolder + "\\" + "ExecutionReport";
            string csvFileName = "request2_" + DateTime.Now.ToString("yyyy_M_d_HH_mm_ss") + ".csv";

            string headers = "\"TransactionID\",\"Quote\",\"Policy\",\"Status\",\"ErrMsg\",\"TotalPremium\",\"PolicyType\",\"PolicySource\",\"FileName\"";
            csv.AppendLine(headers);

            if (!Directory.Exists(executionReportFolder))
            {
                Directory.CreateDirectory(executionReportFolder);
            }

            var outputList = GetFileList(umbrellaValidationFolder, "*.xml");

            foreach (FileInfo outputFile in outputList)
            {
                var rpaResult = xmlHelper.GetOutputResult(outputFile);

                if (rpaResult != null)
                {
                    string datarow = string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\"", 
                        rpaResult.TRANSACTIONGUID, rpaResult.QUOTENUM, rpaResult.POLICYNUM, rpaResult.STATUS, rpaResult.ERRMSG, rpaResult.TOTAL_PREMIUM, rpaResult.POLICYTYPE, rpaResult.POLICYSOURCE, rpaResult.XML_NAME);
                    csv.AppendLine(datarow);
                }
            }

            File.WriteAllText(executionReportFolder + "\\" + csvFileName, csv.ToString());

            return (executionReportFolder + "\\" + csvFileName);
        }

        /// <summary>
        /// Generate Report Card Summary
        /// </summary>
        /// <param name="processedPolicyList"></param>
        /// <returns></returns>
        public IReportCard GetReportCard(IWebAutomateResult webAutomateResult, string processName)
        {
            IReportCard reportCard = new ReportCard();

            reportCard.ProcessName = processName;
            reportCard.TotalFilesCount = (webAutomateResult.ProcessedXml.Count() + webAutomateResult.UnProcessedXml.Count() + webAutomateResult.InvalidXml.Count()).ToString();
            reportCard.SuccessFilesCount = webAutomateResult.ProcessedXml.Count().ToString();
            reportCard.FailureFilesCount = webAutomateResult.UnProcessedXml.Count().ToString();

            return reportCard;
        }

    }
}
