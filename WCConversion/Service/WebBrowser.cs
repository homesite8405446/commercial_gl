﻿using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using SeleniumDriverUpdater;

namespace WCConversion
{
    /// <summary>
    /// Class that contains web functionality
    /// </summary>
    public class WebBrowser
    {
        /// <summary>
        /// Creates a web driver for the Chrome browser
        /// </summary>
        /// <param name="options">Object containing options for the WebBrowser object</param>
        /// <returns>IWebDriver object for Chrome</returns>
        /// <exception cref="System.InvalidCastException">Thrown when driverOptions doesn't match the <see cref="WebBrowserOptions.DriverType"/></exception>
        public ChromeDriver SetupChromeDriver(ChromeOptions chromeOptions, bool updateDriver, string driverPath)
        {
            var setupDriverTask = SetupChromeDriverAsync(chromeOptions, updateDriver, driverPath);

            Task.WhenAll(setupDriverTask).Wait();

            return setupDriverTask.Result;
        }

        /// <summary>
        /// Creates a web driver for the Chrome browser
        /// </summary>
        /// <param name="options">Object containing options for the WebBrowser object</param>
        /// <returns>IWebDriver object for Chrome</returns>
        /// <exception cref="System.InvalidCastException">Thrown when driverOptions doesn't match the <see cref="WebBrowserOptions.DriverType"/></exception>
        public async Task<ChromeDriver> SetupChromeDriverAsync(ChromeOptions chromeOptions, bool updateDriver, string driverPath)
        {
            if (updateDriver)
            {
                DriverInstaller driverInstaller = new DriverInstaller(SeleniumDriverUpdater.WebDriver.ChromeDriver);
                if (!string.IsNullOrWhiteSpace(driverPath))
                {
                    await driverInstaller.UpdateAsync(driverDirectory: driverPath);
                }
                else
                {
                    await driverInstaller.UpdateAsync();
                }
            }

            string driverDirectory = Path.GetDirectoryName(Directory.EnumerateFiles(Path.GetDirectoryName(
                                    Assembly.GetExecutingAssembly().Location), "chromedriver.exe",
                                    SearchOption.AllDirectories).FirstOrDefault());
            ChromeDriverService service = ChromeDriverService.CreateDefaultService(driverDirectory);
            service.HideCommandPromptWindow = true;

            return new ChromeDriver(service, chromeOptions);
        }

    }
}
