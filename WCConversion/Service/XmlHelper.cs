﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace WCConversion
{
    public class XmlHelper
    {
        /// <summary>
        /// Used to deserialize the xml file into class object.
        /// </summary>
        /// <param name="xmlFile"></param>
        /// <returns></returns>
        public InputResult GetInputResult(FileInfo xmlFile)
        {
            InputResult result = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(InputResult));

                StreamReader reader = new StreamReader(xmlFile.FullName);
                result = (InputResult)serializer.Deserialize(reader);
                reader.Close();
            }
            catch {
                result = null;
            }

            return result;
        }

        /// <summary>
        /// Generate output result in xml format
        /// </summary>
        /// <param name="rpaResult"></param>
        /// <param name="fileFullName"></param>
        /// <returns></returns>
        public bool GenerateOutputResult(RPA_RESULTS rpaResult, string fileFullName)
        {
            bool status = true;
            try
            {
                XmlSerializerNamespaces xns = new XmlSerializerNamespaces();
                XmlSerializer xs = new XmlSerializer(typeof(RPA_RESULTS));
                xns.Add(string.Empty, string.Empty);

                using (TextWriter textWriter = new StreamWriter(fileFullName))
                {
                    xs.Serialize(textWriter, rpaResult, xns);
                }
            }
            catch
            {
                status = false;
            }

            return status;
        }

        /// <summary>
        /// Deserialize xml into output results
        /// </summary>
        /// <param name="xmlFile"></param>
        /// <returns></returns>
        public RPA_RESULTS GetOutputResult(FileInfo xmlFile)
        {
            RPA_RESULTS result = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(RPA_RESULTS));

                using (FileStream fs = new FileStream(xmlFile.FullName, FileMode.Open))
                {
                    using (StreamReader reader = new StreamReader(fs))
                    {
                        result = (RPA_RESULTS)serializer.Deserialize(reader);
                        reader.Close();
                    }
                }
            }
            catch
            {
                result = null;
            }

            return result;
        }
    }
}
