﻿using System.Data.SqlClient;

namespace WCConversion
{
    public class LogService
    {
        string _connectionString;

        public LogService(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// Log information into the log table 
        /// </summary>
        /// <param name="botName"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="status"></param>
        /// <param name="remarks"></param>
        /// <param name="serverName"></param>
        /// <returns></returns>
        public int Log(string botName, DateTime startDate, DateTime endDate, string status, string remarks, string serverName)
        {
            int result = -1;
            string subRemarks = remarks.Length >= 200 ? remarks.Substring(0, 199) : remarks;
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                String query = "INSERT INTO dbo.HS_RPA_BotTransactionReport(BotName,StartDate,EndDate,Status,Remarks,Environment) VALUES (@BotName,@StartDate,@EndDate,@Status,@Remarks,@Environment)";

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@BotName", botName);
                    command.Parameters.AddWithValue("@StartDate", startDate);
                    command.Parameters.AddWithValue("@EndDate", endDate);
                    command.Parameters.AddWithValue("@Status", status);
                    command.Parameters.AddWithValue("@Remarks", subRemarks);
                    command.Parameters.AddWithValue("@Environment", serverName);
                    connection.Open();
                    result = command.ExecuteNonQuery();

                    // Check Error
                    //if (result < 0)
                    //    Console.WriteLine("Error inserting data into Database!");
                    return result;
                }
            }
        }

        /// <summary>
        /// Get latest log date by the BOT name from log table
        /// </summary>
        /// <param name="botName"></param>
        /// <returns></returns>
        public DateTime GetLatestSuccessLogDate(string botName)
        {
            DateTime latestDate = DateTime.MinValue;
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                SqlCommand objSqlCommand = new SqlCommand("SELECT TOP(1) StartDate FROM dbo.HS_RPA_BotTransactionReport WHERE BotName='" + botName + "' and Status = 'SUCCESS' ORDER BY StartDate DESC", con);
                try
                {
                    latestDate = Convert.ToDateTime(objSqlCommand.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    con.Close();
                }
            }
            return latestDate;
        }
    }
}
