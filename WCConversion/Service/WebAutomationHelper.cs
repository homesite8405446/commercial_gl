﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDriverManager.DriverConfigs.Impl;
using WebDriverManager.Helpers;
using WebDriverManager;
using OpenQA.Selenium.DevTools.V113.CSS;
using OpenQA.Selenium.Support.UI;
using System.Globalization;
using System.Runtime.Intrinsics.X86;
using Serilog.Parsing;
using Newtonsoft.Json.Linq;
using OpenQA.Selenium.DevTools.V113.Audits;
using System.Security.Principal;
using WildfireMoratorium;

namespace WCConversion
{
    public class WebAutomationHelper
    {
        private readonly ILogger _logger;

        public WebAutomationHelper(ILogger<WebAutomationHelper> logger)
        {
            _logger = logger;
        }

        public IWebAutomateResult AutomateWebsite(Folders folders, List<FileInfo> inputFileList, AppEnvironment appEnv, WebElements webElements, ErrorMsgs errorMsgs, bool oktaLogin, int botRetryCount, bool underlyingAutomaticSelection)
        {
            IWebAutomateResult webAutomateResult = null;
            XmlHelper xmlHelper = new XmlHelper();
            CommonHelper commonHelper = new CommonHelper();
            ChromeOptions chromeOptions = new ChromeOptions();
            WebBrowser webBrowser = new WebBrowser();
            //LogService logService = new LogService();

            //new DriverManager().SetUpDriver(new ChromeConfig(), VersionResolveStrategy.Latest);
            //ChromeDriver chromeDriver = new ChromeDriver();
            ChromeDriver chromeDriver = webBrowser.SetupChromeDriver(chromeOptions, true, string.Empty); //new ChromeDriver();
            IJavaScriptExecutor jsExecutor = (IJavaScriptExecutor)chromeDriver;
            chromeDriver.Manage().Window.Maximize();

            _logger.LogInformation("PAS V11 Application Login START");
            webAutomateResult = LoginIntoPAS(folders, chromeDriver, jsExecutor, appEnv, webElements, oktaLogin, botRetryCount);
            _logger.LogInformation("PAS V11 Application Login END");
            if (webAutomateResult.LoginStatus)
            {
                _logger.LogInformation("Processing each input xml file START");
                foreach (FileInfo file in inputFileList)
                {
                    _logger.LogInformation("Retrieving data from xml file");
                    InputResult inputXmlData = xmlHelper.GetInputResult(file);
                    if (inputXmlData != null)
                    {
                        try
                        {
                            Thread.Sleep(2000);
                            var whatsNewPopupIframe = chromeDriver.FindElements(By.XPath(webElements.WhatsNewPopupIframe));
                            if (whatsNewPopupIframe.Count > 0)
                            {
                                _logger.LogInformation("Handling Whats New Popup - Start");
                                chromeDriver.SwitchTo().Frame(whatsNewPopupIframe[0]);
                                Thread.Sleep(2000);

                                var whatsNewSkipButton = chromeDriver.FindElements(By.XPath(webElements.WhatsNewSkipButton));
                                if (whatsNewSkipButton.Count > 0)
                                {
                                    whatsNewSkipButton[0].Click();
                                    Thread.Sleep(2000);
                                }

                                var whatsNewConfirmButton = chromeDriver.FindElements(By.XPath(webElements.WhatsNewConfirmButton));
                                if (whatsNewConfirmButton.Count > 0)
                                {
                                    whatsNewConfirmButton[0].Click();
                                    Thread.Sleep(2000);
                                }

                                chromeDriver.SwitchTo().DefaultContent();
                                Thread.Sleep(2000);
                                _logger.LogInformation("Handling Whats New Popup - End");
                            }

                            _logger.LogInformation("Creating New Quote for Workers Comp");
                            var ddlNewQuoteArrow = chromeDriver.FindElement(By.XPath(webElements.SelectDropDown));
                            ddlNewQuoteArrow.Click();
                            Thread.Sleep(2000);

                            var workersComp = chromeDriver.FindElement(By.XPath(webElements.WorkersComp));
                            workersComp.Click();
                            Thread.Sleep(2000);

                            var btnLetsBegin = chromeDriver.FindElement(By.XPath(webElements.LetsBegin));
                            btnLetsBegin.Click();
                            Thread.Sleep(2000);
                            WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                            Thread.Sleep(2000);

                            _logger.LogInformation("Automating Quote Screen START");
                            var quoteScreenAutomateStatus = AutomateQuoteScreen(chromeDriver, webElements, folders, inputXmlData, errorMsgs);
                            _logger.LogInformation("Automating Quote Screen END");
                            if (quoteScreenAutomateStatus.Status)
                            {
                                _logger.LogInformation("Automating Producers Screen START");
                                var producersScreenAutomateStatus = AutomateProducersScreen(chromeDriver, webElements, folders, inputXmlData, errorMsgs);
                                _logger.LogInformation("Automating Producers Screen END");
                                if (producersScreenAutomateStatus.Status)
                                {
                                    _logger.LogInformation("Automating Locations Screen START");
                                    var locationsScreenAutomateStatus = AutomateLocationsScreen(chromeDriver, webElements, folders, inputXmlData, errorMsgs);
                                    _logger.LogInformation("Automating Locations Screen END");

                                    _logger.LogInformation("Automating Commercial Umbrella Screen Screen START");
                                    var commercialUmbrellaScreenAutomateStatus = AutomateCommercialUmbrellaScreen(chromeDriver, webElements, folders, inputXmlData, errorMsgs);
                                    commercialUmbrellaScreenAutomateStatus.QuoteNumber = producersScreenAutomateStatus.QuoteNumber;
                                    _logger.LogInformation("Automating Commercial Umbrella Screen END");
                                    if (commercialUmbrellaScreenAutomateStatus.Status)
                                    {
                                        _logger.LogInformation("Automating Underlying Policy Screen START");
                                        var underlyingPolicyScreenAutomateStatus = AutomateUnderlyingPolicyScreen(chromeDriver, webElements, folders, inputXmlData, errorMsgs);
                                        underlyingPolicyScreenAutomateStatus.QuoteNumber = producersScreenAutomateStatus.QuoteNumber;
                                        _logger.LogInformation("Automating Underlying Policy Screen END");
                                        if (underlyingPolicyScreenAutomateStatus.Status)
                                        {
                                            _logger.LogInformation("Automating Optional Provision Screen START");
                                            var optionalProvisionScreenAutomateStatus = AutomateOptionalProvisionScreen(chromeDriver, webElements, folders, inputXmlData, errorMsgs);
                                            optionalProvisionScreenAutomateStatus.QuoteNumber = producersScreenAutomateStatus.QuoteNumber;
                                            _logger.LogInformation("Automating Optional Provision Screen END");
                                            if (optionalProvisionScreenAutomateStatus.Status)
                                            {
                                                _logger.LogInformation("Automating Umbrella Layers Premium Screen START");
                                                var umbrellaLayersPremiumScreenAutomateStatus = AutomateUmbrellaLayersPremiumScreen(chromeDriver, webElements, folders, inputXmlData, errorMsgs);
                                                umbrellaLayersPremiumScreenAutomateStatus.QuoteNumber = producersScreenAutomateStatus.QuoteNumber;
                                                _logger.LogInformation("Automating Umbrella Layers Premium Screen END");
                                                if (umbrellaLayersPremiumScreenAutomateStatus.Status)
                                                {
                                                    _logger.LogInformation("Automating Premium Summary Screen START");
                                                    var premiumSummaryScreenStatus = AutomatePremiumSummaryScreen(chromeDriver, webElements, folders, inputXmlData, errorMsgs, underlyingAutomaticSelection);
                                                    premiumSummaryScreenStatus.QuoteNumber = producersScreenAutomateStatus.QuoteNumber;
                                                    _logger.LogInformation("Automating Premium Summary Screen END");
                                                    if (premiumSummaryScreenStatus.Status)
                                                    {
                                                        commonHelper.GenerateOutputFile(folders.OutputPath, premiumSummaryScreenStatus, inputXmlData, file.Name, folders.WCValidation);
                                                        //Move input file to the processed folder
                                                        commonHelper.MoveFile(folders.InputPath, folders.Processed, file.Name);
                                                        //Add info to the WebAutomateResult
                                                        webAutomateResult.ProcessedXml.Add(inputXmlData);
                                                    }
                                                    else
                                                    {
                                                        //Generate output file with error msg
                                                        commonHelper.GenerateOutputFile(folders.OutputPath, premiumSummaryScreenStatus, inputXmlData, file.Name, folders.WCValidation);
                                                        //Move input file to the unprocessed folder
                                                        commonHelper.MoveFile(folders.InputPath, folders.Unprocessed, file.Name);
                                                        //Add info to the WebAutomateResult
                                                        webAutomateResult.UnProcessedXml.Add(inputXmlData);
                                                        //Log error(txt)
                                                        _logger.LogError("Error Occurred at Umbrella Layers Premium Screen Screen: " + umbrellaLayersPremiumScreenAutomateStatus.ErrMsg);
                                                    }
                                                }
                                                else
                                                {
                                                    //Generate output file with error msg
                                                    commonHelper.GenerateOutputFile(folders.OutputPath, umbrellaLayersPremiumScreenAutomateStatus, inputXmlData, file.Name, folders.WCValidation);
                                                    //Move input file to the unprocessed folder
                                                    commonHelper.MoveFile(folders.InputPath, folders.Unprocessed, file.Name);
                                                    //Add info to the WebAutomateResult
                                                    webAutomateResult.UnProcessedXml.Add(inputXmlData);
                                                }
                                            }
                                            else
                                            {
                                                //Generate output file with error msg
                                                commonHelper.GenerateOutputFile(folders.OutputPath, optionalProvisionScreenAutomateStatus, inputXmlData, file.Name, folders.WCValidation);
                                                //Move input file to the unprocessed folder
                                                commonHelper.MoveFile(folders.InputPath, folders.Unprocessed, file.Name);
                                                //Add info to the WebAutomateResult
                                                webAutomateResult.UnProcessedXml.Add(inputXmlData);
                                                //Log error(txt)
                                                _logger.LogError("Error Occurred at Optional Provision Screen Screen: " + optionalProvisionScreenAutomateStatus.ErrMsg);
                                            }
                                        }
                                        else
                                        {
                                            //Generate output file with error msg
                                            commonHelper.GenerateOutputFile(folders.OutputPath, underlyingPolicyScreenAutomateStatus, inputXmlData, file.Name, folders.WCValidation);
                                            //Move input file to the unprocessed folder
                                            commonHelper.MoveFile(folders.InputPath, folders.Unprocessed, file.Name);
                                            //Add info to the WebAutomateResult
                                            webAutomateResult.UnProcessedXml.Add(inputXmlData);
                                            //Log error(txt)
                                            _logger.LogError("Error Occurred at Underlying Policy Screen Screen: " + underlyingPolicyScreenAutomateStatus.ErrMsg);
                                        }
                                    }
                                    else
                                    {
                                        //Generate output file with error msg
                                        commonHelper.GenerateOutputFile(folders.OutputPath, commercialUmbrellaScreenAutomateStatus, inputXmlData, file.Name, folders.WCValidation);
                                        //Move input file to the unprocessed folder
                                        commonHelper.MoveFile(folders.InputPath, folders.Unprocessed, file.Name);
                                        //Add info to the WebAutomateResult
                                        webAutomateResult.UnProcessedXml.Add(inputXmlData);
                                        //Log error(txt)
                                        _logger.LogError("Error Occurred at Commercial Umbrella Screen Screen: " + commercialUmbrellaScreenAutomateStatus.ErrMsg);
                                    }
                                }
                                else
                                {
                                    //Generate output file with error msg
                                    var rpaResult = commonHelper.GenerateOutputFile(folders.OutputPath, producersScreenAutomateStatus, inputXmlData, file.Name, folders.WCValidation);
                                    //Move input file to the unprocessed folder
                                    commonHelper.MoveFile(folders.InputPath, folders.Unprocessed, file.Name);
                                    //Add info to the WebAutomateResult
                                    webAutomateResult.UnProcessedXml.Add(inputXmlData);
                                    //Log error(txt)
                                    _logger.LogError("Error Occurred at Producers Screen: " + producersScreenAutomateStatus.ErrMsg);
                                }

                            }
                            
                            else
                            {
                                //Generate output file with error msg
                                commonHelper.GenerateOutputFile(folders.OutputPath, quoteScreenAutomateStatus, inputXmlData, file.Name, folders.WCValidation);
                                //Move input file to the unprocessed folder
                                commonHelper.MoveFile(folders.InputPath, folders.Unprocessed, file.Name);
                                //Add info to the WebAutomateResult
                                webAutomateResult.UnProcessedXml.Add(inputXmlData);
                                //Log error(txt)
                                _logger.LogError("Error Occurred at Quote Screen: " + quoteScreenAutomateStatus.ErrMsg);
                            }

                            _logger.LogInformation("Closing Window Popup if there is any before proceeding with new input file.");
                            #region If there is any popup, close all before processing next input file
                            //Back to default content
                            chromeDriver.SwitchTo().DefaultContent();

                            var divWindowPopupClose = chromeDriver.FindElements(By.XPath(webElements.PopupCloseButton));
                            if (divWindowPopupClose.Count > 0)
                            {
                                divWindowPopupClose[0].Click();
                                Thread.Sleep(2000);
                            }

                            var btnYes = chromeDriver.FindElements(By.XPath(webElements.YesButton));
                            if (btnYes.Count > 0)
                            {
                                btnYes[0].Click();
                                Thread.Sleep(2000);
                            }
                            #endregion
                            _logger.LogInformation("Clicking on MAJESCO Icon");
                            var iconMajesco = chromeDriver.FindElement(By.XPath(webElements.IconMajesco));
                            iconMajesco.Click();
                            Thread.Sleep(2000);
                            WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                            Thread.Sleep(2000);

                            //If First Time click is not working
                            var newQuoteDdl = chromeDriver.FindElements(By.XPath(webElements.SelectDropDown));
                            if (newQuoteDdl.Count == 0)
                            {
                                iconMajesco = chromeDriver.FindElement(By.XPath(webElements.IconMajesco));
                                iconMajesco.Click();
                                Thread.Sleep(2000);
                                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                                Thread.Sleep(2000);
                            }
                        }
                        catch (Exception ex)
                        {
                            var automateStatus = new ScreenAutomateStatus();
                            automateStatus.ErrMsg = "Technical Exception Occurred";
                            //Generate output file with error msg
                            commonHelper.GenerateOutputFile(folders.OutputPath, automateStatus, inputXmlData, file.Name, folders.WCValidation);
                            //Move input file to the unprocessed folder
                            commonHelper.MoveFile(folders.InputPath, folders.Unprocessed, file.Name);
                            //Add info to the WebAutomateResult
                            webAutomateResult.UnProcessedXml.Add(inputXmlData);
                            _logger.LogError("Technical Exception Occurred: " + ex.Message);

                            GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                        }
                    }
                    else {
                        //unable to de-serialize
                        _logger.LogInformation("Unable to read the input file: " +file.Name);
                        var automateStatus = new ScreenAutomateStatus();
                        automateStatus.ErrMsg = "Invalid Xml File";
                        //Generate output file with error msg
                        commonHelper.GenerateOutputFile(folders.OutputPath, automateStatus, inputXmlData, file.Name, folders.WCValidation);
                        //Move input file to the unprocessed folder
                        commonHelper.MoveFile(folders.InputPath, folders.Unprocessed, file.Name);
                        //Add info to the WebAutomateResult
                        webAutomateResult.UnProcessedXml.Add(inputXmlData);
                    }
                }
                _logger.LogInformation("Processing each input xml file END");
            }

            #region Closing Browser at the process's end
            if (chromeDriver != null && chromeDriver.SessionId != null)
            {
                chromeDriver.Quit();
            }
            #endregion

            return webAutomateResult;
        }

        private IWebAutomateResult LoginIntoPAS(Folders folders, ChromeDriver chromeDriver, IJavaScriptExecutor jsExecutor, AppEnvironment appEnv, WebElements webElements, bool oktaLogin, int botRetryCount)
        {
            IWebAutomateResult webAutomateResult = new WebAutomateResult();
            CommonHelper commonHelper = new CommonHelper();
            Stopwatch sw = new Stopwatch(); 
            webAutomateResult.LoginStatus = false;
            int i = 1;
            while (true)
            {
                try
                {
                    chromeDriver.Navigate().GoToUrl(appEnv.URL);
                    Thread.Sleep(5000);

                    if (!oktaLogin)
                    {
                        var rdoBtnMajesco = chromeDriver.FindElement(By.XPath(webElements.RdoBtnMajesco));
                        rdoBtnMajesco.Click();
                        Thread.Sleep(1000);

                        var btnMajscoSignIn = chromeDriver.FindElement(By.XPath(webElements.BtnMjescoSignIn));
                        btnMajscoSignIn.Click();
                        Thread.Sleep(1000);

                        var webElementUserName = chromeDriver.FindElement(By.XPath(webElements.InputUserID));
                        webElementUserName.SendKeys(appEnv.UserID);
                        Thread.Sleep(1000);

                        string pwd = commonHelper.GetSecretKey(appEnv.Pwd);
                        var webElementPwd = chromeDriver.FindElement(By.XPath(webElements.InputPassword));
                        webElementPwd.SendKeys(pwd);
                        Thread.Sleep(1000);

                        var chkConsent = chromeDriver.FindElements(By.XPath(webElements.ChkCookiePolicy));
                        if (chkConsent.Count > 0)
                        {
                            if (!chkConsent[0].Selected)
                            {
                                chkConsent[0].Click();
                                Thread.Sleep(1000);
                            }
                        }

                        var loginBt = chromeDriver.FindElement(By.XPath(webElements.BtnLogin));
                        loginBt.Click();
                        Thread.Sleep(6000);
                    }

                    //Check window title as Majesco Policy
                    sw.Reset();
                    sw.Start();
                    while (true)
                    {
                        Thread.Sleep(3000);
                        if (chromeDriver.Title.Contains(appEnv.LoginWindowTitle))
                        {
                            webAutomateResult.LoginStatus = true;
                            break;
                        }

                        if (sw.ElapsedMilliseconds > 40000)
                        {
                            webAutomateResult.ErrorMessage = "Login Exception! It could be portal is down or network issue.";
                            break;
                        }
                    }
                    sw.Stop();

                    if (webAutomateResult.LoginStatus)
                    {
                        break;
                    }
                    else
                    {
                        if (chromeDriver != null && chromeDriver.SessionId != null)
                        {
                            chromeDriver.Quit();
                        }
                    }
                }
                catch (Exception ex)
                {
                    webAutomateResult.ErrorMessage = ex.Message;
                    if (chromeDriver != null && chromeDriver.SessionId != null)
                    {
                        chromeDriver.Quit();
                    }
                }

                if (i >= botRetryCount)
                {
                    //Take Error Screenshot
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, "LoginFailed");
                    if (chromeDriver != null && chromeDriver.SessionId != null)
                    {
                        chromeDriver.Quit();
                    }
                    _logger.LogInformation("Login Failed...");
                    _logger.LogError(webAutomateResult.ErrorMessage);
                    break;  //breaking after after trying specified number of time
                }
                i++;
            }

            return webAutomateResult;
        }

        private IScreenAutomateStatus AutomateQuoteScreen(ChromeDriver chromeDriver, WebElements webElements, Folders folders, InputResult inputXmlData, ErrorMsgs errorMsgs)
        {
            IScreenAutomateStatus screenAutomateStatus = new ScreenAutomateStatus();
            try
            {
                //Navigation variables assignment
                string nav_1 = "Quote";
                string nav_2 = string.Empty;
                string nav_3 = "Quote/Product Information";
                string headingLabelText = string.Empty;
                string errorRemarks = string.Empty;

                //Get quote screen web elements
                QuoteScreen webElementsQuote = webElements.QuoteScreen;

                #region Quote Screen Found / NOT Found check
                var elementHeadingLabel = chromeDriver.FindElements(By.XPath(webElementsQuote.HeadingLabel));
                if (elementHeadingLabel.Count > 0)
                {
                    headingLabelText = elementHeadingLabel[0].Text;
                }

                if (!headingLabelText.Contains(nav_3))
                {
                    errorRemarks = errorMsgs.PageNotFoundException.Replace("%Nav1%", nav_1).Replace("%Nav2%", nav_2).Replace("%Nav3%", nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    return screenAutomateStatus;
                }
                #endregion

                #region Checking if Quote Screen Loaded properly or not
                var inputEffDate = chromeDriver.FindElements(By.XPath(webElementsQuote.EffectiveDate));
                if (inputEffDate.Count == 0)
                {
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = "Quote screen is not loaded properly";
                    return screenAutomateStatus;
                }
                #endregion

                #region Enter effective start date in quote screen
                string fieldName = "Effective Date";
                DateTime dEffDate = DateTime.ParseExact(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.PRIOR_POLICY_EXP_DT, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                inputEffDate[0].SendKeys(dEffDate.ToString("MM/dd/yyyy"));
                inputEffDate[0].SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                #region Check Book Roll Policy Checkbox
                fieldName = "Select BookRoll Policy";
                var chkBookRollPolicy = chromeDriver.FindElement(By.XPath(webElementsQuote.BookRollPolicyCheckbox));
                chkBookRollPolicy.Click();
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                //Navigation variables assignment
                nav_1 = "Quote";
                nav_2 = string.Empty;
                nav_3 = "Insured Information";

                #region Enter Insured Name in quote screen
                fieldName = "Insured Name";
                var inputInsuredName = chromeDriver.FindElement(By.XPath(webElementsQuote.InsuredName));
                inputInsuredName.SendKeys(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.BUSINESS_NAME);
                inputInsuredName.SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                #region Enter Insured type in quote screen
                fieldName = "Insured Type";
                var inputInsuredType = chromeDriver.FindElement(By.XPath(webElementsQuote.InsuredType));
                inputInsuredType.SendKeys(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.BUSINESS_FORM);
                inputInsuredType.SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                #region Enter Business Description in quote screen
                fieldName = "Insured Business Description";
                var inputInsuredBusinessDesc = chromeDriver.FindElement(By.XPath(webElementsQuote.BusinessDescription));
                inputInsuredBusinessDesc.SendKeys(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.BUSINESS_DESC);
                inputInsuredBusinessDesc.SendKeys(Keys.Tab);
                Thread.Sleep(3000);

                #region Check for Sub Window (To be tested)
                var divBusinessDescriptionTable = chromeDriver.FindElements(By.XPath(webElementsQuote.BusinessDescriptionTable));
                if (divBusinessDescriptionTable.Count > 0)
                {
                    bool isFound = false;
                    var spanBusinessDescRows = chromeDriver.FindElements(By.XPath(webElementsQuote.BusinessDescriptionTableRow));
                    foreach (var spanBusinessDescRow in spanBusinessDescRows)
                    {
                        var businessDescVal = spanBusinessDescRow.Text.Trim();
                        if (string.Equals(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.BUSINESS_DESC.ToUpper(), businessDescVal.ToUpper()))
                        {
                            spanBusinessDescRow.FindElement(By.XPath("..")).FindElement(By.XPath("..")).Click();
                            Thread.Sleep(5000);
                            isFound = true;
                            break;
                        }
                    }

                    if (!isFound)
                    {
                        //Click on popup close button if it is there
                        errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                        screenAutomateStatus.Status = false;
                        screenAutomateStatus.ErrMsg = errorRemarks;
                        GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                        return screenAutomateStatus;
                    }
                }
                #endregion

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                #region Enter FEIN Number in quote screen
                fieldName = "FEIN Number";
                var inputFEINNumber = chromeDriver.FindElement(By.XPath(webElementsQuote.FeinNumber));
                inputFEINNumber.SendKeys(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.WC_DATA.BUSINESS_FEIN);
                inputFEINNumber.SendKeys(Keys.Tab);
                Thread.Sleep(3000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                #region Enter Business start year in quote screen
                fieldName = "Insured Business Start Year";
                var inputInsuredBusinessStartYear = chromeDriver.FindElement(By.XPath(webElementsQuote.BusinessStartYear));
                inputInsuredBusinessStartYear.SendKeys(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.BUSINESS_INCEP_YR);
                inputInsuredBusinessStartYear.SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                #region Enter street in quote screen
                fieldName = "Insured Street";
                var inputInsuredStreet = chromeDriver.FindElement(By.XPath(webElementsQuote.InsuredStreet));
                inputInsuredStreet.SendKeys(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.MAIL_ADDR1);
                inputInsuredStreet.SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                #region Enter street_2 in quote screen
                fieldName = "Insured Street 2";
                var inputInsuredStreet2 = chromeDriver.FindElement(By.XPath(webElementsQuote.InsuredStreet_2));
                inputInsuredStreet2.SendKeys(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.MAIL_ADDR2);
                inputInsuredStreet2.SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                #region Enter city in quote screen
                fieldName = "Insured City";
                var inputInsuredCity = chromeDriver.FindElement(By.XPath(webElementsQuote.InsuredCity));
                inputInsuredCity.SendKeys(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.MAIL_ADDR_CITY);
                inputInsuredCity.SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                #region Enter State in quote screen
                fieldName = "Insured State";
                var inputInsuredState = chromeDriver.FindElement(By.XPath(webElementsQuote.InsuredState));
                //Get long form of state
                ExcelHelper excelHelper = new ExcelHelper();
                var stateInfo = excelHelper.GetStateInfo(folders.StateConversionFullFilePath, inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.MAIL_ADDR_ST);
                if (!stateInfo.IsFound)
                {
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = "State is not found in state conversion template";
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                inputInsuredState.SendKeys(stateInfo.LongState);
                inputInsuredState.SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                #region Enter Zip in quote screen
                fieldName = "Insured ZIP";
                var insuredZip = (inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.MAIL_ADDR_ZP).Substring(0, 5);
                var inputInsuredZip = chromeDriver.FindElement(By.XPath(webElementsQuote.InsuredZip));
                inputInsuredZip.SendKeys(insuredZip);
                inputInsuredZip.SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                //Click on Add
                var btnAdd = chromeDriver.FindElement(By.XPath(webElementsQuote.AddButton));
                btnAdd.Click();
                Thread.Sleep(2000);  //Reduce time if not needed 5 secs

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);

                #region Enter Unemployment state in quote screen
                fieldName = "Unemployment State";
                var inputUnemploymentState = chromeDriver.FindElement(By.XPath(webElementsQuote.UnemploymentState));
                ////Get long form of state
                //stateInfo = excelHelper.GetStateInfo(folders.StateConversionFullFilePath, inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.WC_UNEMP_STATE);
                //if (!stateInfo.IsFound)
                //{
                //    screenAutomateStatus.Status = false;
                //    screenAutomateStatus.ErrMsg = "State is not found in state conversion template";
                //    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                //    return screenAutomateStatus;
                //}
                inputUnemploymentState.SendKeys(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.WC_UNEMP_STATE);
                inputUnemploymentState.SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                #region Enter Unemployment number in quote screen
                fieldName = "Unemployment number";
                var inputUnemploymentNumber = chromeDriver.FindElement(By.XPath(webElementsQuote.UnemploymentNumber));
                //inputUnemploymentNumber.SendKeys(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.WC_STATE_UNEMP_ID);
                inputUnemploymentNumber.SendKeys("1");
                inputUnemploymentNumber.SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                //Click on Next
                Thread.Sleep(10000);
                var btnNext = chromeDriver.FindElement(By.XPath(webElementsQuote.NextButton));
                btnNext.Click();
                Thread.Sleep(2000);  //Reduce time if not needed 5 secs

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);

                #region Checking if there is any error popup on clicking on Next button
                var screenErrorPopup = chromeDriver.FindElements(By.XPath(webElementsQuote.PleaseConfirm));
                if (screenErrorPopup.Count > 0)
                {
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    var btnYes = chromeDriver.FindElements(By.XPath(webElementsQuote.YesButton));
                    if (btnYes.Count > 0)
                    {
                        btnYes[0].Click();
                    }
                    Thread.Sleep(3000);
                    errorRemarks = errorMsgs.PageSubmitError.Replace("%Nav1%", nav_1).Replace("%Nav2%", nav_2).Replace("%Nav3%", nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                }
                #endregion

                screenAutomateStatus.Status = true;
            }
            catch (Exception ex)
            {
                screenAutomateStatus.Status = false;
                screenAutomateStatus.ExpMsgTech = ex.Message;
                screenAutomateStatus.ErrMsg = "Technical Exception Occurred at Quote Screen while automating";
                _logger.LogInformation("Technical Exception Occurred at Quote Screen while automating");
                _logger.LogError(ex.Message);
                GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
            }

            return screenAutomateStatus;
        }

        private IScreenAutomateStatus AutomateProducersScreen(ChromeDriver chromeDriver, WebElements webElements, Folders folders, InputResult inputXmlData, ErrorMsgs errorMsgs)
        {
            IScreenAutomateStatus screenAutomateStatus = new ScreenAutomateStatus();
            try
            {
                //Navigation variables assignment
                string nav_1 = "Producers";
                string nav_2 = string.Empty;
                string nav_3 = "Distribution Partner Information";
                string producerLabelText = string.Empty;
                string errorRemarks = string.Empty;

                //Get Producers screen web elements
                ProducersScreen webElementsProducers = webElements.ProducersScreen;

                #region Producers Screen Found / NOT Found check
                var elementProducerLabel = chromeDriver.FindElements(By.XPath(webElementsProducers.ProducerLabel));
                if (elementProducerLabel.Count > 0)
                {
                    producerLabelText = elementProducerLabel[0].Text;
                }

                if (!producerLabelText.Contains(nav_1))
                {
                    errorRemarks = errorMsgs.PageNotFoundException.Replace("%Nav1%", nav_1).Replace("%Nav2%", nav_2).Replace("%Nav3%", nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    return screenAutomateStatus;
                }
                #endregion

                #region Checking if Producers Screen Loaded properly or not
                //comment out if not required.
                try
                {
                    var wait = new WebDriverWait(chromeDriver, TimeSpan.FromSeconds(60000));
                    wait.Until(drv => drv.FindElement(By.XPath(webElementsProducers.QuoteNumber)));
                }
                catch {
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = "Producers screen is not loaded properly";
                    return screenAutomateStatus;
                }
                #endregion

                //Get quote number from producers screen
                var tblQuoteNumber = chromeDriver.FindElement(By.XPath(webElementsProducers.QuoteNumber));
                string quoteNumber = tblQuoteNumber.Text.Split("QUOTE:")[1];
                quoteNumber = quoteNumber.Split("REVISION:")[0].Trim();
                screenAutomateStatus.QuoteNumber = quoteNumber;
                Thread.Sleep(1000);

                #region Enter Distribution Partner Number
                string fieldName = "Partner Number";
                var inputPartnerNumber = chromeDriver.FindElement(By.XPath(webElementsProducers.PartnerNumber));
                inputPartnerNumber.SendKeys(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.DIST_PARTNER_ID);
                inputPartnerNumber.SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                #region Enter Channel in producers screen
                fieldName = "Channel";
                var inputChannel = chromeDriver.FindElement(By.XPath(webElementsProducers.Channel));
                inputChannel.SendKeys(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.CHANNEL);
                inputChannel.SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                #region Enter Promotion in producers screen
                fieldName = "Promotion";
                var inputPromotion = chromeDriver.FindElement(By.XPath(webElementsProducers.Promotion));
                inputPromotion.SendKeys(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.PROMOTION);
                inputPromotion.SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                #region Agency Number in producers screen
                fieldName = "Agency Number";
                var inputAgencyNumber = chromeDriver.FindElement(By.XPath(webElementsProducers.AgencyNumber));
                inputAgencyNumber.SendKeys(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.AGENCY_NUM);
                inputAgencyNumber.SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                #region Enter Partner ID in producers screen
                fieldName = "Partner ID";
                var inputPartnerID = chromeDriver.FindElement(By.XPath(webElementsProducers.PartnerID));
                inputPartnerID.SendKeys(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.PARTNER_ID);
                inputPartnerID.SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                #region Enter contact partner ID in producers screen
                fieldName = "Contact Partner ID";
                var inputContactPartnerID = chromeDriver.FindElement(By.XPath(webElementsProducers.ContactPartnerID));
                inputContactPartnerID.SendKeys(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.CONTACT_PARTNER_ID);
                inputContactPartnerID.SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                #region Enter producers id in producers screen
                fieldName = "Producer ID";
                string userName = WindowsIdentity.GetCurrent().Name;
                userName = userName.Substring(userName.IndexOf("\\") + 1);
                var inputProducerID = chromeDriver.FindElement(By.XPath(webElementsProducers.ProducerID));
                //inputProducerID.SendKeys(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.AGENCY_PRODUCER_ID);
                inputProducerID.SendKeys(userName);
                inputProducerID.SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                //Navigation variables assignment
                nav_1 = "Producers";
                nav_2 = "Supporting Account Information";
                nav_3 = string.Empty;

                #region Enter CDH_ID in producer screen
                fieldName = "CDH ID";
                var inputCDH_ID = chromeDriver.FindElement(By.XPath(webElementsProducers.CdhID));
                inputCDH_ID.SendKeys(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.AMFAM_CDH_ID);
                inputCDH_ID.SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                //Navigation variables assignment
                nav_1 = "Producers";
                nav_2 = string.Empty;
                nav_3 = "Prior Policy Information";

                #region Enter policy number in producer screen
                fieldName = "Prior Policy Number";
                var inputPriorPolicyNumber = chromeDriver.FindElement(By.XPath(webElementsProducers.PriorPolicyNumber));
                inputPriorPolicyNumber.SendKeys(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.PRIOR_POLICY_NO);
                inputPriorPolicyNumber.SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                #region Enter prior annual premium in producer screen
                fieldName = "Prior Annual Premium";
                var inputPriorAnnualPremium = chromeDriver.FindElement(By.XPath(webElementsProducers.PriorAnnualPremium));
                inputPriorAnnualPremium.SendKeys(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.PRIOR_POLICY_ANNUAL_PREM);
                inputPriorAnnualPremium.SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                #region Enter Prior Inception Year in producer screen
                fieldName = "Prior Inception Year";
                var inputPriorInceptionYear = chromeDriver.FindElement(By.XPath(webElementsProducers.PriorInceptionYear));
                inputPriorInceptionYear.SendKeys(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.PRIOR_POLICY_INCEPT_YR);
                inputPriorInceptionYear.SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                #region Enter company in producer screen
                fieldName = "Prior Under Writing Company";
                var inputPriorUWCompany = chromeDriver.FindElement(By.XPath(webElementsProducers.Company));
                inputPriorUWCompany.SendKeys(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.PRIOR_UW_CO);
                inputPriorUWCompany.SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                //Click on Next
                var btnNext = chromeDriver.FindElement(By.XPath(webElementsProducers.NextButton));
                btnNext.Click();
                Thread.Sleep(2000);  //Reduce time if not needed 5 secs

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);

                #region Checking if there is any error popup on clicking on Next button
                var screenErrorPopup = chromeDriver.FindElements(By.XPath(webElementsProducers.PleaseConfirm));
                if (screenErrorPopup.Count > 0)
                {
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    var btnYes = chromeDriver.FindElements(By.XPath(webElementsProducers.YesButton));
                    if (btnYes.Count > 0)
                    {
                        btnYes[0].Click();
                    }
                    Thread.Sleep(3000);
                    errorRemarks = errorMsgs.PageSubmitError.Replace("%Nav1%", nav_1).Replace("%Nav2%", nav_2).Replace("%Nav3%", nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                }
                #endregion

                screenAutomateStatus.Status = true;
            }
            catch (Exception ex)
            {
                screenAutomateStatus.Status = false;
                screenAutomateStatus.ExpMsgTech = ex.Message;
                screenAutomateStatus.ErrMsg = "Technical Exception Occurred at Producers Screen while automating";
                _logger.LogInformation("Technical Exception Occurred at Producers Screen while automating");
                _logger.LogError(ex.Message);
                GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
            }
            return screenAutomateStatus;
        }

        private IScreenAutomateStatus AutomateLocationsScreen(ChromeDriver chromeDriver, WebElements webElements, Folders folders, InputResult inputXmlData, ErrorMsgs errorMsgs)
        {
            IScreenAutomateStatus screenAutomateStatus = new ScreenAutomateStatus();
            try
            {
                //Navigation variables assignment
                string nav_1 = "Locations";
                string nav_2 = string.Empty;
                string nav_3 = "Location Information";
                string fieldName = string.Empty;
                string errorRemarks = string.Empty;

                //Get Location screen web elements
                LocationScreen webElementsLocations = webElements.LocationScreen;
                var locationCount = inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.LOCATIONS;
                for (int i = 0; i < locationCount.Count(); i++)
                {


                    #region Locations Add Screen
                    //Click on Next
                    var btnAdd = chromeDriver.FindElement(By.XPath(webElementsLocations.AddButton));
                    btnAdd.Click();


                    #endregion


                    #region
                    //Get name from location screen
                    var name = chromeDriver.FindElement(By.XPath(webElementsLocations.Name));
                    name.SendKeys(locationCount[i].BUSINESS_NAME);
                    name.SendKeys(Keys.Tab);
                    Thread.Sleep(1000);
                    #endregion

                    #region Enter Street name
                    fieldName = "Street Name";
                    var inputStreet = chromeDriver.FindElement(By.XPath(webElementsLocations.Street));
                    inputStreet.SendKeys(locationCount[i].BUSINESS_ADDR_1);
                    inputStreet.SendKeys(Keys.Tab);
                    Thread.Sleep(1000);

                    //Wait for loading bar to be disappeared
                    WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                    //Check In Progress Div Error Status
                    if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                    {
                        errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                        screenAutomateStatus.Status = false;
                        screenAutomateStatus.ErrMsg = errorRemarks;
                        GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                        return screenAutomateStatus;
                    }
                    #endregion

                    #region Enter Street2 in Locations screen

                    var inputStreet2 = chromeDriver.FindElement(By.XPath(webElementsLocations.Street2));
                    inputStreet2.SendKeys(locationCount[i].BUSINESS_ADDR_2);
                    inputStreet2.SendKeys(Keys.Tab);
                    Thread.Sleep(1000);

                    #endregion
                    #region City in locations screen
                    fieldName = "Location City";
                    var inputCity = chromeDriver.FindElement(By.XPath(webElementsLocations.City));
                    inputCity.SendKeys(locationCount[i].BUSINESS_ADDR_CITY);
                    inputCity.SendKeys(Keys.Tab);
                    Thread.Sleep(1000);


                    //Wait for loading bar to be disappeared
                    WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                    //Check In Progress Div Error Status
                    if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                    {
                        errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                        screenAutomateStatus.Status = false;
                        screenAutomateStatus.ErrMsg = errorRemarks;
                        GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                        return screenAutomateStatus;
                    }
                    #endregion

                    #region Enter Location State in Locations screen
                    fieldName = "Location State";
                    var inputState =chromeDriver.FindElement(By.XPath(webElementsLocations.State));
                    //Get long form of state
                    ExcelHelper excelHelper = new ExcelHelper();
                    var stateInfo = excelHelper.GetStateInfo(folders.StateConversionFullFilePath, locationCount[i].BUSINESS_ADDR_ST);
                    if (!stateInfo.IsFound)
                    {
                        screenAutomateStatus.Status = false;
                        screenAutomateStatus.ErrMsg = "State is not found in state conversion template";
                        GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                        return screenAutomateStatus;
                    }

                    inputState.SendKeys(stateInfo.LongState);

                    inputState.SendKeys(Keys.Tab);
                    
                    
                    Thread.Sleep(1000);

                    //Wait for loading bar to be disappeared
                    WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                    //Check In Progress Div Error Status
                    if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                    {
                        errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                        screenAutomateStatus.Status = false;
                        screenAutomateStatus.ErrMsg = errorRemarks;
                        GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                        return screenAutomateStatus;
                    }
                    #endregion

                    #region Enter Zip in Locations screen
                    fieldName = "Zip";
                    var inputZip = chromeDriver.FindElement(By.XPath(webElementsLocations.Zip));
                    inputZip.SendKeys(locationCount[i].BUSINESS_ADDR_ZIP);
                    inputZip.SendKeys(Keys.Tab);
                    Thread.Sleep(1000);

                    //Wait for loading bar to be disappeared
                    WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                    //Check In Progress Div Error Status
                    if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                    {
                        errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                        screenAutomateStatus.Status = false;
                        screenAutomateStatus.ErrMsg = errorRemarks;
                        GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                        return screenAutomateStatus;
                    }
                    #endregion


                    //Click on Next
                    var btnNext = chromeDriver.FindElement(By.XPath(webElementsLocations.NextButton));
                    btnNext.Click();
                    Thread.Sleep(2000);  //Reduce time if not needed 5 secs
                }
                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);

                #region Checking if there is any error popup on clicking on Next button
                var screenErrorPopup = chromeDriver.FindElements(By.XPath(webElementsLocations.PleaseConfirm));
                if (screenErrorPopup.Count > 0)
                {
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    var btnYes = chromeDriver.FindElements(By.XPath(webElementsLocations.YesButton));
                    if (btnYes.Count > 0)
                    {
                        btnYes[0].Click();
                    }
                    Thread.Sleep(3000);
                    errorRemarks = errorMsgs.PageSubmitError.Replace("%Nav1%", nav_1).Replace("%Nav2%", nav_2).Replace("%Nav3%", nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                }
                #endregion

                screenAutomateStatus.Status = true;
            }
            catch (Exception ex)
            {
                screenAutomateStatus.Status = false;
                screenAutomateStatus.ExpMsgTech = ex.Message;
                screenAutomateStatus.ErrMsg = "Technical Exception Occurred at Location Screen while automating";
                _logger.LogInformation("Technical Exception Occurred at Location Screen while automating");
                _logger.LogError(ex.Message);
                GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
            }
            return screenAutomateStatus;
        }


        private IScreenAutomateStatus AutomateCommercialUmbrellaScreen(ChromeDriver chromeDriver, WebElements webElements, Folders folders, InputResult inputXmlData, ErrorMsgs errorMsgs)
        {
            IScreenAutomateStatus screenAutomateStatus = new ScreenAutomateStatus();
            try
            {
                //Navigation variables assignment
                string nav_1 = "Commercial Umbrella";
                string nav_2 = string.Empty;
                string nav_3 = string.Empty;
                string comUmbLabelText = string.Empty;
                string errorRemarks = string.Empty;

                //Get Commercial Umbrella screen web elements
                CommercialUmbrellaScreen webElementsCommercialUmbrella = webElements.CommercialUmbrellaScreen;

                #region Commercial Umbrella Screen Found/NOT Found check
                var elementComUmbLabel = chromeDriver.FindElements(By.XPath(webElementsCommercialUmbrella.ComUmbLabel));
                if (elementComUmbLabel.Count > 0)
                {
                    comUmbLabelText = elementComUmbLabel[0].Text;
                }

                if (!comUmbLabelText.Contains(nav_1))
                {
                    errorRemarks = errorMsgs.PageNotFoundException.Replace("%Nav1%", nav_1).Replace("%Nav2%", nav_2).Replace("%Nav3%", nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    return screenAutomateStatus;
                }
                #endregion

                #region Checking if Commercial Umbrella Screen Loaded properly or not
                //comment out if not required.
                try
                {
                    var wait = new WebDriverWait(chromeDriver, TimeSpan.FromSeconds(60000));
                    wait.Until(drv => drv.FindElement(By.XPath(webElementsCommercialUmbrella.State)));
                }
                catch
                {
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = "Commercial Umbrella screen is not loaded properly";
                    return screenAutomateStatus;
                }
                #endregion

                #region Enter state in commercail umbrella screen
                string fieldName = "Commercial Umbrella : State";
                var inputState = chromeDriver.FindElement(By.XPath(webElementsCommercialUmbrella.State));

                //Get long form of state
                ExcelHelper excelHelper = new ExcelHelper();
                var stateInfo = excelHelper.GetStateInfo(folders.StateConversionFullFilePath, inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.PRIMARY_STATE);
                if (!stateInfo.IsFound)
                {
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = "State is not found in state conversion template";
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                inputState.Clear();
                Thread.Sleep(1000);
                inputState.SendKeys(stateInfo.LongState);
                inputState.SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                #region Enter Commercial Umbrella : Each Occurrence Limit
                fieldName = "Commercial Umbrella : Each Occurrence Limit";
                var inputOccurrenceLimit = chromeDriver.FindElement(By.XPath(webElementsCommercialUmbrella.OccurrenceLimit));

                inputOccurrenceLimit.Clear();
                Thread.Sleep(1000);

                inputOccurrenceLimit.SendKeys(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.UMB_EACH_OCCURRENCE_LIMIT);
                inputOccurrenceLimit.SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                #region Check if each occurrence limit window popup exist or not
                var divOccurenceLimitTable = chromeDriver.FindElements(By.XPath(webElementsCommercialUmbrella.OccurenceLimitTable));
                if (divOccurenceLimitTable.Count > 0)
                {
                    bool isFound = false;
                    var spanOccurranceLimitRows = chromeDriver.FindElements(By.XPath(webElementsCommercialUmbrella.OccurenceLimitTableRow));
                    foreach (var spanOccurranceLimitRow in spanOccurranceLimitRows)
                    {
                        var limitVal = spanOccurranceLimitRow.Text.Trim().Replace(" ", "").Replace("\n", "").Replace("\r", "");
                        if (string.Equals(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.UMB_EACH_OCCURRENCE_LIMIT, limitVal))
                        {
                            spanOccurranceLimitRow.FindElement(By.XPath("..")).FindElement(By.XPath("..")).Click();
                            Thread.Sleep(5000);
                            isFound = true;
                            break;
                        }
                    }

                    if (!isFound)
                    {
                        errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                        screenAutomateStatus.Status = false;
                        screenAutomateStatus.ErrMsg = errorRemarks;
                        GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                        return screenAutomateStatus;
                    }
                }

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                #region Enter Commercial Umbrella : Self Insured Retention
                fieldName = "Commercial Umbrella : Self Insured Retention";
                var inputSelfInsuredRetention= chromeDriver.FindElement(By.XPath(webElementsCommercialUmbrella.SelfInsuredRetention));

                inputSelfInsuredRetention.Clear();
                Thread.Sleep(1000);

                inputSelfInsuredRetention.SendKeys(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.SELFINSURED_RETENTION);
                inputSelfInsuredRetention.SendKeys(Keys.Tab);
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                //Click on Next
                var btnNext = chromeDriver.FindElement(By.XPath(webElementsCommercialUmbrella.NextButton));
                btnNext.Click();
                Thread.Sleep(2000);  //Reduce time if not needed 5 secs

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);

                #region Checking if there is any error popup on clicking on Next button
                var screenErrorPopup = chromeDriver.FindElements(By.XPath(webElementsCommercialUmbrella.PleaseConfirm));
                if (screenErrorPopup.Count > 0)
                {
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    var btnYes = chromeDriver.FindElements(By.XPath(webElementsCommercialUmbrella.YesButton));
                    if (btnYes.Count > 0)
                    {
                        btnYes[0].Click();
                    }
                    Thread.Sleep(3000);
                    errorRemarks = errorMsgs.PageSubmitError.Replace("%Nav1%", nav_1).Replace("%Nav2%", nav_2).Replace("%Nav3%", nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                }
                #endregion

                screenAutomateStatus.Status = true;
            }
            catch (Exception ex)
            {
                screenAutomateStatus.Status = false;
                screenAutomateStatus.ExpMsgTech = ex.Message;
                screenAutomateStatus.ErrMsg = "Technical Exception Occurred at Commercial Umbrella Screen while automating";
                _logger.LogInformation("Technical Exception Occurred at Commercial Umbrella Screen while automating");
                _logger.LogError(ex.Message);
                GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
            }
            return screenAutomateStatus;
        }

        private IScreenAutomateStatus AutomateUnderlyingPolicyScreen(ChromeDriver chromeDriver, WebElements webElements, Folders folders, InputResult inputXmlData, ErrorMsgs errorMsgs)
        {
            IScreenAutomateStatus screenAutomateStatus = new ScreenAutomateStatus();
            try
            {
                //Navigation variables assignment
                string nav_1 = "Underlying Policy Details";
                string nav_2 = "Add";
                string nav_3 = string.Empty;
                string underlyingPolLabelText = string.Empty;
                string errorRemarks = string.Empty;

                //Get Underlying Policy screen web elements
                UnderlyingPolicyScreen webElementsUnderlyingPol = webElements.UnderlyingPolicyScreen;

                #region Underlying Policy Screen Found/NOT Found check
                var elementUnderlyingLabel = chromeDriver.FindElements(By.XPath(webElementsUnderlyingPol.UnderlyingPolicyLabel));
                if (elementUnderlyingLabel.Count > 0)
                {
                    underlyingPolLabelText = elementUnderlyingLabel[0].Text;
                }

                if (!underlyingPolLabelText.Contains(nav_1))
                {
                    errorRemarks = errorMsgs.PageNotFoundException.Replace("%Nav1%", nav_1).Replace("%Nav2%", nav_2).Replace("%Nav3%", nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    return screenAutomateStatus;
                }
                #endregion

                var otherPolicies = inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.OTHER_POLICIES;
                for (int i = 0; i < otherPolicies.Count; i++)
                {
                    string polDetails = "Policy_" + (i+1).ToString() + "_details";
                    nav_3 = polDetails;
                    string fieldName = "Add";
                    var btnAdd = chromeDriver.FindElement(By.XPath(webElementsUnderlyingPol.Add));
                    btnAdd.Click();
                    Thread.Sleep(2000);

                    #region To check policy details page is loaded properly or not with retry logic
                    bool lineOfBusinessLabelExists = false;
                    int count = 0;
                    while (count < 4)
                    {
                        var lineOfBusinessLabel = chromeDriver.FindElements(By.XPath(webElementsUnderlyingPol.LineOfBusinessLabel));
                        if (lineOfBusinessLabel.Count > 0 && lineOfBusinessLabel[0].Text.Contains("Line of Business"))
                        {
                            lineOfBusinessLabelExists = true;
                            break;
                        }
                        else {
                            Thread.Sleep(2000);
                            count++;
                        }
                    }

                    if (!lineOfBusinessLabelExists)
                    {
                        errorRemarks = errorMsgs.PageNotFoundException.Replace("%Nav1%", nav_1).Replace("%Nav2%", nav_2).Replace("%Nav3%", nav_3);
                        screenAutomateStatus.Status = false;
                        screenAutomateStatus.ErrMsg = errorRemarks;
                        return screenAutomateStatus;
                    }
                    #endregion

                    #region Enter Underlying : Line of Business
                    fieldName = "Underlying : Line of Business";
                    var inputLineOfBusiness = chromeDriver.FindElement(By.XPath(webElementsUnderlyingPol.LineOfBusiness));

                    inputLineOfBusiness.Clear();
                    Thread.Sleep(1000);

                    inputLineOfBusiness.SendKeys(otherPolicies[i].POLICY_LOB);
                    inputLineOfBusiness.SendKeys(Keys.Tab);
                    Thread.Sleep(1000);

                    //Wait for loading bar to be disappeared
                    WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                    //Check In Progress Div Error Status
                    if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                    {
                        errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                        screenAutomateStatus.Status = false;
                        screenAutomateStatus.ErrMsg = errorRemarks;
                        GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                        return screenAutomateStatus;
                    }
                    #endregion

                    #region Check if Line of Business window popup exist or not
                    var divLineOfBusinessTable = chromeDriver.FindElements(By.XPath(webElementsUnderlyingPol.LineOfBusinessTable));
                    if (divLineOfBusinessTable.Count > 0)
                    {
                        bool isFound = false;
                        var spanLOBRows = chromeDriver.FindElements(By.XPath(webElementsUnderlyingPol.LineOfBusinessTableRow));
                        foreach (var spanLOBRow in spanLOBRows)
                        {
                            var lobVal = spanLOBRow.Text.Trim();
                            if (string.Equals(otherPolicies[i].POLICY_LOB.ToUpper(), lobVal.ToUpper()))
                            {
                                spanLOBRow.FindElement(By.XPath("..")).FindElement(By.XPath("..")).Click();
                                Thread.Sleep(5000);
                                isFound = true;
                                break;
                            }
                        }

                        if (!isFound)
                        {
                            errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                            screenAutomateStatus.Status = false;
                            screenAutomateStatus.ErrMsg = errorRemarks;
                            GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                            return screenAutomateStatus;
                        }
                    }

                    //Wait for loading bar to be disappeared
                    WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                    //Check In Progress Div Error Status
                    if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                    {
                        errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                        screenAutomateStatus.Status = false;
                        screenAutomateStatus.ErrMsg = errorRemarks;
                        GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                        return screenAutomateStatus;
                    }
                    #endregion

                    #region Enter Underlying : Underlying Policy
                    fieldName = "Underlying : Underlying Policy";

                    if (!string.IsNullOrEmpty(inputXmlData.DF_CORR_MASTER.POLICYSOURCE) && inputXmlData.DF_CORR_MASTER.POLICYSOURCE.Trim().ToUpper() == "AMFAM")
                    {
                        var rdoBtnManual = chromeDriver.FindElement(By.XPath(webElementsUnderlyingPol.ManualRadioButton));
                        rdoBtnManual.Click();
                    }
                    else
                    {
                        if (string.Equals(otherPolicies[i].POLICY_SETUP_TYPE, "Automatic"))
                        {
                            var rdoBtnAutomatic = chromeDriver.FindElement(By.XPath(webElementsUnderlyingPol.AutomaticRadioButton));
                            rdoBtnAutomatic.Click();
                        }
                        else if (string.Equals(otherPolicies[i].POLICY_SETUP_TYPE, "Manual"))
                        {
                            var rdoBtnManual = chromeDriver.FindElement(By.XPath(webElementsUnderlyingPol.ManualRadioButton));
                            rdoBtnManual.Click();
                        }
                    }
                    Thread.Sleep(1000);

                    //Wait for loading bar to be disappeared
                    WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                    //Check In Progress Div Error Status
                    if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                    {
                        errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                        screenAutomateStatus.Status = false;
                        screenAutomateStatus.ErrMsg = errorRemarks;
                        GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                        return screenAutomateStatus;
                    }
                    #endregion

                    #region Enter Underlying : Policy Number
                    fieldName = "Underlying : Policy Number";
                    var inputPolicyNumber = chromeDriver.FindElement(By.XPath(webElementsUnderlyingPol.PolicyNumber));

                    inputPolicyNumber.Clear();
                    Thread.Sleep(1000);

                    inputPolicyNumber.SendKeys(otherPolicies[i].POLICY_NUMBER);
                    inputPolicyNumber.SendKeys(Keys.Tab);
                    Thread.Sleep(1000);

                    //Wait for loading bar to be disappeared
                    WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                    //Check In Progress Div Error Status
                    if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                    {
                        errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                        screenAutomateStatus.Status = false;
                        screenAutomateStatus.ErrMsg = errorRemarks;
                        GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                        return screenAutomateStatus;
                    }
                    #endregion

                    if (string.Equals(otherPolicies[i].POLICY_SETUP_TYPE.ToUpper(), "MANUAL"))
                    {
                        #region Enter Underlying : Policy Type
                        fieldName = "Underlying : Policy Type";

                        if (string.Equals(otherPolicies[i].POLICY_TYPE, "Policy"))
                        {
                            var rdoBtnPolicy = chromeDriver.FindElement(By.XPath(webElementsUnderlyingPol.PolicyRadioButton));
                            rdoBtnPolicy.Click();
                        }
                        else if (string.Equals(otherPolicies[i].POLICY_SETUP_TYPE, "Quote"))
                        {
                            var rdoBtnQuote = chromeDriver.FindElement(By.XPath(webElementsUnderlyingPol.QuoteRadioButton));
                            rdoBtnQuote.Click();
                        }
                        Thread.Sleep(1000);

                        //Wait for loading bar to be disappeared
                        WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                        //Check In Progress Div Error Status
                        if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                        {
                            errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                            screenAutomateStatus.Status = false;
                            screenAutomateStatus.ErrMsg = errorRemarks;
                            GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                            return screenAutomateStatus;
                        }
                        #endregion

                        #region Enter Underlying : Underwriting Company
                        fieldName = "Underlying : Underwriting Company";
                        var inputUnderwritingCompany = chromeDriver.FindElement(By.XPath(webElementsUnderlyingPol.UnderWritingCompany));

                        inputUnderwritingCompany.Clear();
                        Thread.Sleep(1000);

                        inputUnderwritingCompany.SendKeys(otherPolicies[i].POLICY_UW_COMPANY);
                        inputUnderwritingCompany.SendKeys(Keys.Tab);
                        Thread.Sleep(1000);

                        //Wait for loading bar to be disappeared
                        WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                        //Check In Progress Div Error Status
                        if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                        {
                            errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                            screenAutomateStatus.Status = false;
                            screenAutomateStatus.ErrMsg = errorRemarks;
                            GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                            return screenAutomateStatus;
                        }
                        #endregion

                        #region Enter Underlying : Effective Date
                        fieldName = "Underlying : Effective Date";
                        DateTime dEffDate = DateTime.ParseExact(otherPolicies[i].EFFECTIVE_DATE, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                        var inputEffDate = chromeDriver.FindElement(By.XPath(webElementsUnderlyingPol.EffectiveDate));

                        inputEffDate.Clear();
                        Thread.Sleep(1000);

                        inputEffDate.SendKeys(dEffDate.ToString("MM/dd/yyyy"));
                        inputEffDate.SendKeys(Keys.Tab);
                        Thread.Sleep(1000);

                        //Wait for loading bar to be disappeared
                        WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                        //Check In Progress Div Error Status
                        if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                        {
                            errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                            screenAutomateStatus.Status = false;
                            screenAutomateStatus.ErrMsg = errorRemarks;
                            GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                            return screenAutomateStatus;
                        }
                        #endregion

                        #region Enter Underlying : Expiration Date
                        fieldName = "Underlying : Expiration Date";
                        DateTime dExpDate = DateTime.ParseExact(otherPolicies[i].EXPIRE_DATE, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                        var inputExpDate = chromeDriver.FindElement(By.XPath(webElementsUnderlyingPol.ExpirationDate));

                        inputExpDate.Clear();
                        Thread.Sleep(1000);

                        inputExpDate.SendKeys(dExpDate.ToString("MM/dd/yyyy"));
                        inputExpDate.SendKeys(Keys.Tab);
                        Thread.Sleep(1000);

                        //Wait for loading bar to be disappeared
                        WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                        //Check In Progress Div Error Status
                        if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                        {
                            errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                            screenAutomateStatus.Status = false;
                            screenAutomateStatus.ErrMsg = errorRemarks;
                            GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                            return screenAutomateStatus;
                        }
                        #endregion

                        #region Enter Underlying : Policy Status
                        fieldName = "Underlying : Policy Status";
                        var inputPolicyStatus = chromeDriver.FindElement(By.XPath(webElementsUnderlyingPol.PolicyStatus));

                        inputPolicyStatus.Clear();
                        Thread.Sleep(1000);

                        inputPolicyStatus.SendKeys(otherPolicies[i].POLICY_STATUS);
                        inputPolicyStatus.SendKeys(Keys.Tab);
                        Thread.Sleep(1000);

                        //Wait for loading bar to be disappeared
                        WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                        //Check In Progress Div Error Status
                        if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                        {
                            errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                            screenAutomateStatus.Status = false;
                            screenAutomateStatus.ErrMsg = errorRemarks;
                            GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                            return screenAutomateStatus;
                        }
                        #endregion

                        #region Enter Underlying : BOPOverridden Liability Limit per Occurence
                        if (string.Equals(otherPolicies[i].POLICY_LOB.Trim().ToUpper(), "BUSINESS OWNERS"))
                        {
                            fieldName = "Underlying : BOPOverridden Liability Limit per Occurrence " + otherPolicies[i].BO_OCCUR_LIMIT;
                            var inputBopLiabilityLimit = chromeDriver.FindElement(By.XPath(webElementsUnderlyingPol.BopLiabilityLimit));

                            inputBopLiabilityLimit.Clear();
                            Thread.Sleep(1000);

                            inputBopLiabilityLimit.SendKeys(otherPolicies[i].BO_OCCUR_LIMIT);
                            inputBopLiabilityLimit.SendKeys(Keys.Tab);
                            Thread.Sleep(4000);

                            #region Check for Sub Window (To be tested)
                            var divBopLiabilityLimitTable = chromeDriver.FindElements(By.XPath(webElementsUnderlyingPol.BopLiabilityLimitTable));
                            if (divBopLiabilityLimitTable.Count > 0)
                            {
                                bool isFound = false;
                                var spanBopLLRows = chromeDriver.FindElements(By.XPath(webElementsUnderlyingPol.BopLiabilityLimitTableRow));
                                foreach (var spanBopLLRow in spanBopLLRows)
                                {
                                    var bopLLVal = spanBopLLRow.Text.Trim().Replace(" ", "").Replace("\n", "").Replace("\r", "");
                                    if (string.Equals(otherPolicies[i].BO_OCCUR_LIMIT.ToUpper(), bopLLVal.ToUpper()))
                                    {
                                        spanBopLLRow.FindElement(By.XPath("..")).FindElement(By.XPath("..")).Click();
                                        Thread.Sleep(5000);
                                        isFound = true;
                                        break;
                                    }
                                }

                                if (!isFound)
                                {
                                    //Click on popup close button if it is there
                                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                                    screenAutomateStatus.Status = false;
                                    screenAutomateStatus.ErrMsg = errorRemarks;
                                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                                    return screenAutomateStatus;
                                }
                            }

                            //Wait for loading bar to be disappeared
                            WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                            //Check In Progress Div Error Status
                            if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                            {
                                errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                                screenAutomateStatus.Status = false;
                                screenAutomateStatus.ErrMsg = errorRemarks;
                                GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                                return screenAutomateStatus;
                            }
                            #endregion
                        }
                        #endregion

                        #region Enter Underlying : CAOverridden Liability Limit per Occurence
                        if (string.Equals(otherPolicies[i].POLICY_LOB.Trim().ToUpper(), "COMMERCIAL AUTO"))
                        {
                            fieldName = "Underlying : CAOverridden Liability Limit per Occurrence " + otherPolicies[i].AU_OCCUR_LIMIT;
                            var inputCaLiabilityLimit = chromeDriver.FindElement(By.XPath(webElementsUnderlyingPol.CaLiabilityLimit));

                            inputCaLiabilityLimit.Clear();
                            Thread.Sleep(1000);

                            inputCaLiabilityLimit.SendKeys(otherPolicies[i].AU_OCCUR_LIMIT);
                            inputCaLiabilityLimit.SendKeys(Keys.Tab);
                            Thread.Sleep(4000);

                            #region Check for Sub Window (To be tested)
                            var divCaLiabilityLimitTable = chromeDriver.FindElements(By.XPath(webElementsUnderlyingPol.CaLiabilityLimitTable));
                            if (divCaLiabilityLimitTable.Count > 0)
                            {
                                bool isFound = false;
                                var spanCaLLRows = chromeDriver.FindElements(By.XPath(webElementsUnderlyingPol.CaLiabilityLimitTableRow));
                                foreach (var spanCaLLRow in spanCaLLRows)
                                {
                                    var caLLVal = spanCaLLRow.Text.Trim().Replace(" ", "").Replace("\n", "").Replace("\r", "");
                                    if (string.Equals(otherPolicies[i].AU_OCCUR_LIMIT.ToUpper(), caLLVal.ToUpper()))
                                    {
                                        spanCaLLRow.FindElement(By.XPath("..")).FindElement(By.XPath("..")).Click();
                                        Thread.Sleep(5000);
                                        isFound = true;
                                        break;
                                    }
                                }

                                if (!isFound)
                                {
                                    //Click on popup close button if it is there
                                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                                    screenAutomateStatus.Status = false;
                                    screenAutomateStatus.ErrMsg = errorRemarks;
                                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                                    return screenAutomateStatus;
                                }
                            }

                            //Wait for loading bar to be disappeared
                            WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                            //Check In Progress Div Error Status
                            if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                            {
                                errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                                screenAutomateStatus.Status = false;
                                screenAutomateStatus.ErrMsg = errorRemarks;
                                GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                                return screenAutomateStatus;
                            }
                            #endregion
                        }
                        #endregion

                        #region Enter Underlying : CPOverridden Liability Limit per Occurence
                        if (string.Equals(otherPolicies[i].POLICY_LOB.Trim().ToUpper(), "GENERAL LIABILITY"))
                        {
                            fieldName = "Underlying : CPOverridden Liability Limit per Occurrence " + otherPolicies[i].CP_OCCUR_LIMIT;
                            var inputCpLiabilityLimit = chromeDriver.FindElement(By.XPath(webElementsUnderlyingPol.CpLiabilityLimit));

                            inputCpLiabilityLimit.Clear();
                            Thread.Sleep(1000);

                            inputCpLiabilityLimit.SendKeys(otherPolicies[i].CP_OCCUR_LIMIT);
                            inputCpLiabilityLimit.SendKeys(Keys.Tab);
                            Thread.Sleep(4000);

                            #region Check for Sub Window (To be tested)
                            var divCpLiabilityLimitTable = chromeDriver.FindElements(By.XPath(webElementsUnderlyingPol.CpLiabilityLimitTable));
                            if (divCpLiabilityLimitTable.Count > 0)
                            {
                                bool isFound = false;
                                var spanCpLLRows = chromeDriver.FindElements(By.XPath(webElementsUnderlyingPol.CpLiabilityLimitTableRow));
                                foreach (var spanCpLLRow in spanCpLLRows)
                                {
                                    var cpLLVal = spanCpLLRow.Text.Trim().Replace(" ", "").Replace("\n","").Replace("\r","");
                                    if (string.Equals(otherPolicies[i].CP_OCCUR_LIMIT.ToUpper(), cpLLVal.ToUpper()))
                                    {
                                        spanCpLLRow.FindElement(By.XPath("..")).FindElement(By.XPath("..")).Click();
                                        Thread.Sleep(5000);
                                        isFound = true;
                                        break;
                                    }
                                }

                                if (!isFound)
                                {
                                    //Click on popup close button if it is there
                                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                                    screenAutomateStatus.Status = false;
                                    screenAutomateStatus.ErrMsg = errorRemarks;
                                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                                    return screenAutomateStatus;
                                }
                            }

                            //Wait for loading bar to be disappeared
                            WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                            //Check In Progress Div Error Status
                            if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                            {
                                errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                                screenAutomateStatus.Status = false;
                                screenAutomateStatus.ErrMsg = errorRemarks;
                                GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                                return screenAutomateStatus;
                            }
                            #endregion
                        }
                        #endregion

                        #region Enter Underlying : WCOverridden Liability Limit per Occurence
                        if (string.Equals(otherPolicies[i].POLICY_LOB.Trim().ToUpper(), "EMPLOYERS LIABILITY"))
                        {
                            fieldName = "Underlying : WCOverridden Liability Limit per Occurrence " + otherPolicies[i].WC_EMPL_LIAB_LIMIT;
                            var inputWcLiabilityLimit = chromeDriver.FindElement(By.XPath(webElementsUnderlyingPol.WcLiabilityLimit));

                            inputWcLiabilityLimit.Clear();
                            Thread.Sleep(1000);

                            inputWcLiabilityLimit.SendKeys(otherPolicies[i].WC_EMPL_LIAB_LIMIT);
                            inputWcLiabilityLimit.SendKeys(Keys.Tab);
                            Thread.Sleep(4000);

                            #region Check for Sub Window (To be tested)
                            var divWcLiabilityLimitTable = chromeDriver.FindElements(By.XPath(webElementsUnderlyingPol.WcLiabilityLimitTable));
                            if (divWcLiabilityLimitTable.Count > 0)
                            {
                                bool isFound = false;
                                var spanWcLLRows = chromeDriver.FindElements(By.XPath(webElementsUnderlyingPol.WcLiabilityLimitTableRow));
                                foreach (var spanWcLLRow in spanWcLLRows)
                                {
                                    var wcLLVal = spanWcLLRow.Text.Trim().Replace(" ", "").Replace("\n", "").Replace("\r", "");
                                    if (string.Equals(otherPolicies[i].WC_EMPL_LIAB_LIMIT.ToUpper(), wcLLVal.ToUpper()))
                                    {
                                        spanWcLLRow.FindElement(By.XPath("..")).FindElement(By.XPath("..")).Click();
                                        Thread.Sleep(5000);
                                        isFound = true;
                                        break;
                                    }
                                }

                                if (!isFound)
                                {
                                    //Click on popup close button if it is there
                                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                                    screenAutomateStatus.Status = false;
                                    screenAutomateStatus.ErrMsg = errorRemarks;
                                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                                    return screenAutomateStatus;
                                }
                            }

                            //Wait for loading bar to be disappeared
                            WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                            //Check In Progress Div Error Status
                            if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                            {
                                errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                                screenAutomateStatus.Status = false;
                                screenAutomateStatus.ErrMsg = errorRemarks;
                                GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                                return screenAutomateStatus;
                            }
                            #endregion
                        }
                        #endregion
                    }

                    //Click on Next
                    var btnNext = chromeDriver.FindElement(By.XPath(webElementsUnderlyingPol.NextButton));
                    btnNext.Click();
                    Thread.Sleep(2000);  //Reduce time if not needed 5 secs

                    //Wait for loading bar to be disappeared
                    WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);

                    #region Checking if there is any error popup on clicking on Next button
                    var screenErrorPopup1 = chromeDriver.FindElements(By.XPath(webElementsUnderlyingPol.PleaseConfirm));
                    if (screenErrorPopup1.Count > 0)
                    {
                        GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                        var btnYes = chromeDriver.FindElements(By.XPath(webElementsUnderlyingPol.YesButton));
                        if (btnYes.Count > 0)
                        {
                            btnYes[0].Click();
                        }
                        Thread.Sleep(3000);
                        errorRemarks = errorMsgs.PageSubmitError.Replace("%Nav1%", nav_1).Replace("%Nav2%", nav_2).Replace("%Nav3%", nav_3);
                        screenAutomateStatus.Status = false;
                        screenAutomateStatus.ErrMsg = errorRemarks;
                        return screenAutomateStatus;
                    }
                    #endregion
                }

                //Click on hide element
                var hideElement = chromeDriver.FindElement(By.XPath(webElementsUnderlyingPol.HideElement));
                hideElement.Click();
                Thread.Sleep(1000);

              

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);

                #region Checking if there is any error popup on clicking on Done button
                var screenErrorPopup = chromeDriver.FindElements(By.XPath(webElementsUnderlyingPol.PleaseConfirm));
                if (screenErrorPopup.Count > 0)
                {
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    var btnYes = chromeDriver.FindElements(By.XPath(webElementsUnderlyingPol.YesButton));
                    if (btnYes.Count > 0)
                    {
                        btnYes[0].Click();
                    }
                    Thread.Sleep(3000);
                    errorRemarks = errorMsgs.PageSubmitError.Replace("%Nav1%", nav_1).Replace("%Nav2%", nav_2).Replace("%Nav3%", nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    return screenAutomateStatus;
                }
                #endregion
                //Click on the done with underlying policy details button
                var doneButton = chromeDriver.FindElement(By.XPath(webElementsUnderlyingPol.DoneButton));
                doneButton.Click();
                Thread.Sleep(1000);
                screenAutomateStatus.Status = true;
            }
            catch (Exception ex)
            {
                screenAutomateStatus.Status = false;
                screenAutomateStatus.ExpMsgTech = ex.Message;
                screenAutomateStatus.ErrMsg = "Technical Exception Occurred at Underlying Policy Screen while automating";
                _logger.LogInformation("Technical Exception Occurred at Underlying Policy Screen while automating");
                _logger.LogError(ex.Message);
                GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
            }
            return screenAutomateStatus;
        }

        private IScreenAutomateStatus AutomateOptionalProvisionScreen(ChromeDriver chromeDriver, WebElements webElements, Folders folders, InputResult inputXmlData, ErrorMsgs errorMsgs)
        {
            IScreenAutomateStatus screenAutomateStatus = new ScreenAutomateStatus();
            try
            {
                //Navigation variables assignment
                string nav_1 = "Optional Provisions";
                string nav_2 = "Add";
                string nav_3 = string.Empty;
                string optionalProvisionLabelText = string.Empty;
                string errorRemarks = string.Empty;

                //Get optional provision screen web elements
                OptionalProvisionScreen webElementsOptionalProvision = webElements.OptionalProvisionScreen;

                #region optional provision Screen Found/NOT Found check
                var elementOptionalProvisionLabel = chromeDriver.FindElements(By.XPath(webElementsOptionalProvision.OptionalProvisionLabel));
                if (elementOptionalProvisionLabel.Count > 0)
                {
                    optionalProvisionLabelText = elementOptionalProvisionLabel[0].Text;
                }

                if (!optionalProvisionLabelText.Contains(nav_1))
                {
                    errorRemarks = errorMsgs.PageNotFoundException.Replace("%Nav1%", nav_1).Replace("%Nav2%", nav_2).Replace("%Nav3%", nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    return screenAutomateStatus;
                }
                #endregion

                var forms = inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.FORMS;
                for (int i = 0; i < forms.Count; i++)
                {
                    string formDetails = "Form_" + (i + 1).ToString() + "_details";
                    nav_3 = formDetails;
                    string fieldName = "Add";
                    var btnAdd = chromeDriver.FindElement(By.XPath(webElementsOptionalProvision.Add));
                    btnAdd.Click();
                    Thread.Sleep(2000);

                    #region To check form description page is loaded properly or not with retry logic
                    bool formDescriptionLabelExists = false;
                    int count = 0;
                    while (count < 4)
                    {
                        var formDescriptionLabel = chromeDriver.FindElements(By.XPath(webElementsOptionalProvision.FormDescriptionLabel));
                        if (formDescriptionLabel.Count > 0 && formDescriptionLabel[0].Text.Contains("Form Description"))
                        {
                            formDescriptionLabelExists = true;
                            break;
                        }
                        else
                        {
                            Thread.Sleep(2000);
                            count++;
                        }
                    }

                    if (!formDescriptionLabelExists)
                    {
                        errorRemarks = errorMsgs.PageNotFoundException.Replace("%Nav1%", nav_1).Replace("%Nav2%", nav_2).Replace("%Nav3%", nav_3);
                        screenAutomateStatus.Status = false;
                        screenAutomateStatus.ErrMsg = errorRemarks;
                        return screenAutomateStatus;
                    }
                    #endregion

                    #region Manual Form Entry Required
                    string[] manualFormCodeList = { "CU 24 19", "CU 21 11", "CU 24 17", "CU 24 07" };
                    if (manualFormCodeList.Contains(forms[i].FORM_CODE.ToUpper()))
                    {
                        errorRemarks = "Manual Form Entry Required";
                        screenAutomateStatus.Status = false;
                        screenAutomateStatus.ErrMsg = errorRemarks;
                        return screenAutomateStatus;
                    }
                    #endregion

                    #region Enter OptionalProvisions : Form Desc
                    fieldName = "OptionalProvisions : Form Desc";
                    var inputFormDescription = chromeDriver.FindElement(By.XPath(webElementsOptionalProvision.FormDescription));

                    inputFormDescription.Clear();
                    Thread.Sleep(1000);

                    inputFormDescription.SendKeys(forms[i].FORM_NAME);
                    inputFormDescription.SendKeys(Keys.Tab);
                    Thread.Sleep(1000);

                    //Wait for loading bar to be disappeared
                    WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                    //Check In Progress Div Error Status
                    if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                    {
                        errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                        screenAutomateStatus.Status = false;
                        screenAutomateStatus.ErrMsg = errorRemarks;
                        GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                        return screenAutomateStatus;
                    }
                    #endregion

                    #region Check for sub window
                    var divFormDescriptionTable = chromeDriver.FindElements(By.XPath(webElementsOptionalProvision.FormDescriptionTable));
                    if (divFormDescriptionTable.Count > 0)
                    {
                        bool isFound = false;
                        var spanFormDescriptionTableRows = chromeDriver.FindElements(By.XPath(webElementsOptionalProvision.FormDescriptionTableRow));
                        foreach (var spanFormDescriptionTableRow in spanFormDescriptionTableRows)
                        {
                            var elementFormDesc = spanFormDescriptionTableRow.FindElement(By.XPath(webElementsOptionalProvision.FormDescriptionTableRowFormDesc));
                            var elementFormNum = spanFormDescriptionTableRow.FindElement(By.XPath(webElementsOptionalProvision.FormDescriptionTableRowFormNum));

                            var formDescVal = elementFormDesc.Text.Trim();
                            var formNumVal = elementFormNum.Text.Trim();
                            if (string.Equals(forms[i].FORM_NAME.ToUpper(), formDescVal.ToUpper()) || string.Equals(forms[i].FORM_CODE.ToUpper(), formNumVal.ToUpper()))
                            {
                                elementFormDesc.FindElement(By.XPath("..")).FindElement(By.XPath("..")).Click();
                                Thread.Sleep(2000);
                                isFound = true;
                                break;
                            }
                        }

                        if (!isFound)
                        {
                            errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                            screenAutomateStatus.Status = false;
                            screenAutomateStatus.ErrMsg = errorRemarks;
                            GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                            return screenAutomateStatus;
                        }
                    }

                    //Wait for loading bar to be disappeared
                    WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                    //Check In Progress Div Error Status
                    if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                    {
                        errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                        screenAutomateStatus.Status = false;
                        screenAutomateStatus.ErrMsg = errorRemarks;
                        GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                        return screenAutomateStatus;
                    }
                    #endregion

                    #region Optional Provision - Prompt1 Entries
                    //Verify the input value
                    #region Enter Optional Provisions : Description of your work
                    var inputDecsOfWork = chromeDriver.FindElements(By.XPath(webElementsOptionalProvision.DescOfWork));
                    if (inputDecsOfWork.Count > 0)
                    {
                        fieldName = "Optional Provisions : Description of your work";

                        inputDecsOfWork[0].Clear();
                        Thread.Sleep(1000);

                        inputDecsOfWork[0].SendKeys(forms[i].FORM_PROMPT1);
                        inputDecsOfWork[0].SendKeys(Keys.Tab);
                        Thread.Sleep(1000);

                        //Wait for loading bar to be disappeared
                        WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                        //Check In Progress Div Error Status
                        if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                        {
                            errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                            screenAutomateStatus.Status = false;
                            screenAutomateStatus.ErrMsg = errorRemarks;
                            GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                            return screenAutomateStatus;
                        }
                    }
                    #endregion

                    //Verify the input value
                    #region Enter Optional Provisions : Description of Operations
                    var inputDecsOfOps = chromeDriver.FindElements(By.XPath(webElementsOptionalProvision.DescOfOperations));
                    if (inputDecsOfOps.Count > 0)
                    {
                        fieldName = "Optional Provisions : Description of Operations";

                        inputDecsOfOps[0].Clear();
                        Thread.Sleep(1000);

                        inputDecsOfOps[0].SendKeys(forms[i].FORM_PROMPT1);
                        inputDecsOfOps[0].SendKeys(Keys.Tab);
                        Thread.Sleep(1000);

                        //Wait for loading bar to be disappeared
                        WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                        //Check In Progress Div Error Status
                        if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                        {
                            errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                            screenAutomateStatus.Status = false;
                            screenAutomateStatus.ErrMsg = errorRemarks;
                            GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                            return screenAutomateStatus;
                        }
                    }
                    #endregion

                    //Verify the input value
                    #region Enter Optional Provisions : Description and Location of Premises
                    var inputDecsLocationOfPremises = chromeDriver.FindElements(By.XPath(webElementsOptionalProvision.DecsLocationOfPremises));
                    if (inputDecsLocationOfPremises.Count > 0)
                    {
                        fieldName = "Optional Provisions : Description and Location of Premises";

                        inputDecsLocationOfPremises[0].Clear();
                        Thread.Sleep(1000);

                        inputDecsLocationOfPremises[0].SendKeys(forms[i].FORM_PROMPT1);
                        inputDecsLocationOfPremises[0].SendKeys(Keys.Tab);
                        Thread.Sleep(1000);

                        //Wait for loading bar to be disappeared
                        WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                        //Check In Progress Div Error Status
                        if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                        {
                            errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                            screenAutomateStatus.Status = false;
                            screenAutomateStatus.ErrMsg = errorRemarks;
                            GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                            return screenAutomateStatus;
                        }
                    }
                    #endregion

                    //Verify the input value
                    #region Enter Optional Provisions : Location & Description of Operations
                    var inputLocationDescOfOps = chromeDriver.FindElements(By.XPath(webElementsOptionalProvision.LocationDescOfOps));
                    if (inputLocationDescOfOps.Count > 0)
                    {
                        fieldName = "Optional Provisions : Location & Description of Operations";

                        inputLocationDescOfOps[0].Clear();
                        Thread.Sleep(1000);

                        inputLocationDescOfOps[0].SendKeys(forms[i].FORM_PROMPT1);
                        inputLocationDescOfOps[0].SendKeys(Keys.Tab);
                        Thread.Sleep(1000);

                        //Wait for loading bar to be disappeared
                        WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                        //Check In Progress Div Error Status
                        if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                        {
                            errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                            screenAutomateStatus.Status = false;
                            screenAutomateStatus.ErrMsg = errorRemarks;
                            GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                            return screenAutomateStatus;
                        }
                    }
                    #endregion

                    //Verify the input value
                    #region Enter Optional Provisions : Description Of Ongoing Operation(s)
                    var inputDecsOfOngoingOps = chromeDriver.FindElements(By.XPath(webElementsOptionalProvision.DecsOfOngoingOps));
                    if (inputDecsOfOngoingOps.Count > 0)
                    {
                        fieldName = "Optional Provisions : Description Of Ongoing Operation(s)";

                        inputDecsOfOngoingOps[0].Clear();
                        Thread.Sleep(1000);

                        inputDecsOfOngoingOps[0].SendKeys(forms[i].FORM_PROMPT1);
                        inputDecsOfOngoingOps[0].SendKeys(Keys.Tab);
                        Thread.Sleep(1000);

                        //Wait for loading bar to be disappeared
                        WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                        //Check In Progress Div Error Status
                        if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                        {
                            errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                            screenAutomateStatus.Status = false;
                            screenAutomateStatus.ErrMsg = errorRemarks;
                            GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                            return screenAutomateStatus;
                        }
                    }
                    #endregion

                    //Verify the input value
                    #region Enter Optional Provisions : Description of Premises and Operations
                    var inputDecsOfPremisesOps = chromeDriver.FindElements(By.XPath(webElementsOptionalProvision.DecsOfPremisesOps));
                    if (inputDecsOfPremisesOps.Count > 0)
                    {
                        fieldName = "Optional Provisions : Description of Premises and Operations";

                        inputDecsOfPremisesOps[0].Clear();
                        Thread.Sleep(1000);

                        inputDecsOfPremisesOps[0].SendKeys(forms[i].FORM_PROMPT1);
                        inputDecsOfPremisesOps[0].SendKeys(Keys.Tab);
                        Thread.Sleep(1000);

                        //Wait for loading bar to be disappeared
                        WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                        //Check In Progress Div Error Status
                        if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                        {
                            errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                            screenAutomateStatus.Status = false;
                            screenAutomateStatus.ErrMsg = errorRemarks;
                            GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                            return screenAutomateStatus;
                        }
                    }
                    #endregion

                    //Verify the input value
                    #region Enter Optional Provisions : Name of Additional Insured Person(s) or Organization(s)
                    var inputPersonOrgName = chromeDriver.FindElements(By.XPath(webElementsOptionalProvision.PersonOrgName));
                    if (inputPersonOrgName.Count > 0)
                    {
                        fieldName = "Optional Provisions : Name of Additional Insured Person(s) or Organization(s)";

                        inputPersonOrgName[0].Clear();
                        Thread.Sleep(1000);

                        inputPersonOrgName[0].SendKeys(forms[i].FORM_PROMPT1);
                        inputPersonOrgName[0].SendKeys(Keys.Tab);
                        Thread.Sleep(1000);

                        //Wait for loading bar to be disappeared
                        WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                        //Check In Progress Div Error Status
                        if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                        {
                            errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                            screenAutomateStatus.Status = false;
                            screenAutomateStatus.ErrMsg = errorRemarks;
                            GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                            return screenAutomateStatus;
                        }
                    }
                    #endregion
                    #endregion

                    #region Optional Provision - Prompt2 Entries
                    //Verify the input value
                    #region Enter Optional Provisions : Excluded Hazards(s)
                    var inputExcludedHazards = chromeDriver.FindElements(By.XPath(webElementsOptionalProvision.ExcludedHazards));
                    if (inputExcludedHazards.Count > 0)
                    {
                        fieldName = "Optional Provisions : Excluded Hazards(s)";

                        inputExcludedHazards[0].Clear();
                        Thread.Sleep(1000);

                        inputExcludedHazards[0].SendKeys(forms[i].FORM_PROMPT2);
                        inputExcludedHazards[0].SendKeys(Keys.Tab);
                        Thread.Sleep(1000);

                        //Wait for loading bar to be disappeared
                        WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                        //Check In Progress Div Error Status
                        if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                        {
                            errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                            screenAutomateStatus.Status = false;
                            screenAutomateStatus.ErrMsg = errorRemarks;
                            GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                            return screenAutomateStatus;
                        }
                    }
                    #endregion

                    //Verify the input value
                    #region Enter Optional Provisions : Covered Hazard(s)
                    var inputCoveredHazards = chromeDriver.FindElements(By.XPath(webElementsOptionalProvision.CoveredHazard));
                    if (inputCoveredHazards.Count > 0)
                    {
                        fieldName = "Optional Provisions : Covered Hazard(s)";

                        inputCoveredHazards[0].Clear();
                        Thread.Sleep(1000);

                        inputCoveredHazards[0].SendKeys(forms[i].FORM_PROMPT2);
                        inputCoveredHazards[0].SendKeys(Keys.Tab);
                        Thread.Sleep(1000);

                        //Wait for loading bar to be disappeared
                        WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                        //Check In Progress Div Error Status
                        if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                        {
                            errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                            screenAutomateStatus.Status = false;
                            screenAutomateStatus.ErrMsg = errorRemarks;
                            GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                            return screenAutomateStatus;
                        }
                    }
                    #endregion

                    //Verify the input value
                    #region Enter Optional Provisions : Specific Location
                    var inputSpecificLocation = chromeDriver.FindElements(By.XPath(webElementsOptionalProvision.SpecificLocation));
                    if (inputSpecificLocation.Count > 0)
                    {
                        fieldName = "Optional Provisions : Specific Location";

                        inputSpecificLocation[0].Clear();
                        Thread.Sleep(1000);

                        inputSpecificLocation[0].SendKeys(forms[i].FORM_PROMPT2);
                        inputSpecificLocation[0].SendKeys(Keys.Tab);
                        Thread.Sleep(1000);

                        //Wait for loading bar to be disappeared
                        WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                        //Check In Progress Div Error Status
                        if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                        {
                            errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                            screenAutomateStatus.Status = false;
                            screenAutomateStatus.ErrMsg = errorRemarks;
                            GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                            return screenAutomateStatus;
                        }
                    }
                    #endregion
                    #endregion

                    //Click on Next
                    var btnNext = chromeDriver.FindElement(By.XPath(webElementsOptionalProvision.NextButton));
                    btnNext.Click();
                    Thread.Sleep(2000);  //Reduce time if not needed 5 secs

                    //Wait for loading bar to be disappeared
                    WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);

                    #region Checking if there is any error popup on clicking on Next button
                    var screenErrorPopup1 = chromeDriver.FindElements(By.XPath(webElementsOptionalProvision.PleaseConfirm));
                    if (screenErrorPopup1.Count > 0)
                    {
                        GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                        var btnYes = chromeDriver.FindElements(By.XPath(webElementsOptionalProvision.YesButton));
                        if (btnYes.Count > 0)
                        {
                            btnYes[0].Click();
                        }
                        Thread.Sleep(3000);
                        errorRemarks = errorMsgs.PageSubmitError.Replace("%Nav1%", nav_1).Replace("%Nav2%", nav_2).Replace("%Nav3%", nav_3);
                        screenAutomateStatus.Status = false;
                        screenAutomateStatus.ErrMsg = errorRemarks;
                        return screenAutomateStatus;
                    }
                    #endregion
                }

                //Click on hide element
                var hideElement = chromeDriver.FindElement(By.XPath(webElementsOptionalProvision.HideElement));
                hideElement.Click();
                Thread.Sleep(1000);

                //Click on the done with underlying policy details button
                var doneButton = chromeDriver.FindElement(By.XPath(webElementsOptionalProvision.DoneButton));
                doneButton.Click();
                Thread.Sleep(1000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);

                #region Checking if there is any error popup on clicking on Done button
                var screenErrorPopup = chromeDriver.FindElements(By.XPath(webElementsOptionalProvision.PleaseConfirm));
                if (screenErrorPopup.Count > 0)
                {
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    var btnYes = chromeDriver.FindElements(By.XPath(webElementsOptionalProvision.YesButton));
                    if (btnYes.Count > 0)
                    {
                        btnYes[0].Click();
                    }
                    Thread.Sleep(3000);
                    errorRemarks = errorMsgs.PageSubmitError.Replace("%Nav1%", nav_1).Replace("%Nav2%", nav_2).Replace("%Nav3%", nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    return screenAutomateStatus;
                }
                #endregion

                screenAutomateStatus.Status = true;
            }
            catch (Exception ex)
            {
                screenAutomateStatus.Status = false;
                screenAutomateStatus.ExpMsgTech = ex.Message;
                screenAutomateStatus.ErrMsg = "Technical Exception Occurred at Optional Provision Screen while automating";
                _logger.LogInformation("Technical Exception Occurred at Optional Provision Screen while automating");
                _logger.LogError(ex.Message);
                GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
            }
            return screenAutomateStatus;
        }

        private IScreenAutomateStatus AutomateUmbrellaLayersPremiumScreen(ChromeDriver chromeDriver, WebElements webElements, Folders folders, InputResult inputXmlData, ErrorMsgs errorMsgs)
        {
            IScreenAutomateStatus screenAutomateStatus = new ScreenAutomateStatus();
            try
            {
                //Navigation variables assignment
                string nav_1 = "Umbrella Layers Premium";
                string nav_2 = "Add";
                string nav_3 = string.Empty;
                string umbrellaLayersPremiumLabelText = string.Empty;
                string errorRemarks = string.Empty;

                //Get Umbrella Layers Premium screen web elements
                UmbrellaLayersPremiumScreen webElementsUmbrellaLayersPremium = webElements.UmbrellaLayersPremiumScreen;

                #region Umbrella Layers Premium Screen Found/NOT Found check
                var elementUmbrellaLayersPremiumLabel = chromeDriver.FindElements(By.XPath(webElementsUmbrellaLayersPremium.UmbrellaLayersPremiumLabel));
                if (elementUmbrellaLayersPremiumLabel.Count > 0)
                {
                    umbrellaLayersPremiumLabelText = elementUmbrellaLayersPremiumLabel[0].Text;
                }

                if (!umbrellaLayersPremiumLabelText.Contains(nav_1))
                {
                    errorRemarks = errorMsgs.PageNotFoundException.Replace("%Nav1%", nav_1).Replace("%Nav2%", nav_2).Replace("%Nav3%", nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    return screenAutomateStatus;
                }
                #endregion

                var layerPremiums = inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.LAYER_PREMIUMS;
                for (int i = 0; i < layerPremiums.Count; i++)
                {
                    string premiumDetails = "Premium_" + (i + 1).ToString() + "_details";
                    nav_3 = premiumDetails;

                    #region Enter Umbrella Layers Premium : Layer Premium
                    string fieldName = "Umbrella Layers Premium : Layer Premium";
                    var inputLayersPremium = chromeDriver.FindElements(By.XPath(webElementsUmbrellaLayersPremium.LayersPremium.Replace("{RowIndex}", (i+1).ToString())));

                    if (inputLayersPremium.Count > 0)
                    {
                        inputLayersPremium[0].Clear();
                        Thread.Sleep(1000);

                        inputLayersPremium[0].SendKeys(layerPremiums[i].LAYER_PREMIUM);
                        inputLayersPremium[0].SendKeys(Keys.Tab);
                        Thread.Sleep(1000);

                        //Wait for loading bar to be disappeared
                        WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                        //Check In Progress Div Error Status
                        if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                        {
                            errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                            screenAutomateStatus.Status = false;
                            screenAutomateStatus.ErrMsg = errorRemarks;
                            GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                            return screenAutomateStatus;
                        }
                    }
                    #endregion
                }

                //Click on Next
                var btnNext = chromeDriver.FindElement(By.XPath(webElementsUmbrellaLayersPremium.NextButton));
                btnNext.Click();
                Thread.Sleep(2000);  //Reduce time if not needed 5 secs

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);

                #region Checking if there is any error popup on clicking on Next button
                var screenErrorPopup = chromeDriver.FindElements(By.XPath(webElementsUmbrellaLayersPremium.PleaseConfirm));
                if (screenErrorPopup.Count > 0)
                {
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    var btnYes = chromeDriver.FindElements(By.XPath(webElementsUmbrellaLayersPremium.YesButton));
                    if (btnYes.Count > 0)
                    {
                        btnYes[0].Click();
                    }
                    Thread.Sleep(3000);
                    errorRemarks = errorMsgs.PageSubmitError.Replace("%Nav1%", nav_1).Replace("%Nav2%", nav_2).Replace("%Nav3%", nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                }
                #endregion

                screenAutomateStatus.Status = true;
            }
            catch (Exception ex)
            {
                screenAutomateStatus.Status = false;
                screenAutomateStatus.ExpMsgTech = ex.Message;
                screenAutomateStatus.ErrMsg = "Technical Exception Occurred at Umbrella Layers Premium Screen while automating";
                _logger.LogInformation("Technical Exception Occurred at Umbrella Layers Premium Screen while automating");
                _logger.LogError(ex.Message);
                GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
            }
            return screenAutomateStatus;
        }

        private IScreenAutomateStatus AutomatePremiumSummaryScreen(ChromeDriver chromeDriver, WebElements webElements, Folders folders, InputResult inputXmlData, ErrorMsgs errorMsgs, bool underlyingAutomaticSelection)
        {
            IScreenAutomateStatus screenAutomateStatus = new ScreenAutomateStatus();
            try
            {
                //Navigation variables assignment
                string nav_1 = "Premium Summary";
                string nav_2 = string.Empty;
                string nav_3 = string.Empty;
                string premiumSummaryLabelText = string.Empty;
                string errorRemarks = string.Empty;

                //Get Premium Summary Screen screen web elements
                PremiumSummaryScreen webElementsPremiumSummaryScreen = webElements.PremiumSummaryScreen;

                #region Premium Summary Screen Found/NOT Found check
                var elementUmbrellaLayersPremiumLabel = chromeDriver.FindElements(By.XPath(webElementsPremiumSummaryScreen.PremiumSummaryLabel));
                if (elementUmbrellaLayersPremiumLabel.Count > 0)
                {
                    premiumSummaryLabelText = elementUmbrellaLayersPremiumLabel[0].Text;
                }

                if (!premiumSummaryLabelText.Contains(nav_1))
                {
                    errorRemarks = errorMsgs.PageNotFoundException.Replace("%Nav1%", nav_1).Replace("%Nav2%", nav_2).Replace("%Nav3%", nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    return screenAutomateStatus;
                }
                #endregion

                #region Click on Calculate Premium element
                string fieldName = "Quote : Click on Calculate Premium";
                var inputCalculatePremium = chromeDriver.FindElement(By.XPath(webElementsPremiumSummaryScreen.CalculatePremium));
                inputCalculatePremium.Click();
                Thread.Sleep(2000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }
                #endregion

                nav_1 = "Premium Summary";
                nav_2 = "Quote";
                nav_3 = string.Empty;

                #region General information Screen Found/NOT Found check
                string generalInformationLabelText = string.Empty;
                string totalPremium = string.Empty;
                var iFramePremiumSummary = chromeDriver.FindElements(By.XPath(webElementsPremiumSummaryScreen.IFramePremiumSummary));
                if (iFramePremiumSummary!= null && iFramePremiumSummary.Count > 0)
                {
                    chromeDriver.SwitchTo().Frame(iFramePremiumSummary[0]);
                    var elementGeneralInformationLabel = chromeDriver.FindElements(By.XPath(webElementsPremiumSummaryScreen.GeneralInformationLabel));
                    if (elementGeneralInformationLabel.Count > 0)
                    {
                        generalInformationLabelText = elementGeneralInformationLabel[0].Text;
                    }
                    var elementTotalPremium = chromeDriver.FindElements(By.XPath(webElementsPremiumSummaryScreen.TotalPremium));
                    if (elementTotalPremium.Count > 0)
                    { 
                        totalPremium = elementTotalPremium[0].Text;
                    }
                    chromeDriver.SwitchTo().DefaultContent();
                }

                screenAutomateStatus.TotalPremium = totalPremium;

                if (!generalInformationLabelText.Contains("General Information"))
                {
                    errorRemarks = errorMsgs.PageNotFoundException.Replace("%Nav1%", nav_1).Replace("%Nav2%", nav_2).Replace("%Nav3%", nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    return screenAutomateStatus;
                }
                #endregion

                //Click on Next
                var btnNext = chromeDriver.FindElement(By.XPath(webElementsPremiumSummaryScreen.NextButton));
                btnNext.Click();
                Thread.Sleep(8000);

                //Switch to Dashboard
                var iframeDashboard = FindElementByMaxWait(chromeDriver, webElements.IframeDashboard, 40);
                chromeDriver.SwitchTo().Frame(iframeDashboard);

                //Navigation variables assignment
                nav_1 = "Quote Status : Dashboard";
                nav_2 = "Pending Quote";
                nav_3 = string.Empty;

                //Assign field name
                fieldName = "Quote: Pending Quote Status Check";

                //Wait for pending quote status
                bool status = WaitForTargetStatus(chromeDriver, webElementsPremiumSummaryScreen.Status, nav_2, 60);
                if (!status) {
                    errorRemarks = errorMsgs.PageNotFoundException.Replace("%Nav1%", nav_1).Replace("%Nav2%", nav_2).Replace("%Nav3%", nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    return screenAutomateStatus;
                }

                if (!string.IsNullOrEmpty(inputXmlData.DF_CORR_MASTER.POLICYSOURCE) && inputXmlData.DF_CORR_MASTER.POLICYSOURCE.Trim().ToUpper() == "MSA")
                {
                    if (!string.IsNullOrEmpty(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.EXCEPTION))
                    {
                        screenAutomateStatus.Status = false;
                        screenAutomateStatus.ErrMsg = inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.EXCEPTION;
                        return screenAutomateStatus;
                    }
                }

                #region Pending Premium Input
                if (!string.IsNullOrEmpty(inputXmlData.DF_CORR_MASTER.POLICYSOURCE) && inputXmlData.DF_CORR_MASTER.POLICYSOURCE.Trim().ToUpper() == "AMFAM")
                {
                    if (!underlyingAutomaticSelection)
                    {
                        errorRemarks = "Pending Premium Input";
                        screenAutomateStatus.StatusPartial = true;
                        screenAutomateStatus.ErrMsg = errorRemarks;
                        return screenAutomateStatus;
                    }
                }
                #endregion

                //Click on Convert to Policy button
                fieldName = "Quote : Click on Convert to policy";
                var elementConvertToPolicy = chromeDriver.FindElement(By.XPath(webElementsPremiumSummaryScreen.ConvertToPolicy));
                elementConvertToPolicy.Click();
                Thread.Sleep(7000);

                //Back to default content
                chromeDriver.SwitchTo().DefaultContent();

                //Navigation variables assignment
                nav_1 = "Policy";
                nav_2 = "Payment Plan";
                nav_3 = string.Empty;

                //Wait for policy screens status
                status = WaitForTargetStatus(chromeDriver, webElementsPremiumSummaryScreen.PolicyLabel, nav_1, 30);
                if (!status)
                {
                    errorRemarks = errorMsgs.PageNotFoundException.Replace("%Nav1%", nav_1).Replace("%Nav2%", nav_2).Replace("%Nav3%", nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    return screenAutomateStatus;
                }

                var elementFullPayTransactionPremium = chromeDriver.FindElement(By.XPath(webElementsPremiumSummaryScreen.FullPayTransactionPremium));
                var fullPayTransPremTxt = elementFullPayTransactionPremium.GetAttribute("value").Trim().Replace("$", "").Replace(" ", "").Replace(",", "");
                double fullPayTransactionPremium = Convert.ToDouble(fullPayTransPremTxt);

                #region Enter Payment Plan
                string billCycle = fullPayTransactionPremium <= 550.00 ? "Annual Pay" : "Monthly";
                fieldName = "Payment Plan";
                var elementPaymentPlan = chromeDriver.FindElement(By.XPath(webElementsPremiumSummaryScreen.PaymentPlan));
                elementPaymentPlan.Clear();
                Thread.Sleep(1000);
                elementPaymentPlan.SendKeys(billCycle);
                Thread.Sleep(2000);

                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);

                string paymentPlan = elementPaymentPlan.GetAttribute("value").Trim();
                if (string.IsNullOrEmpty(paymentPlan))
                {
                    elementPaymentPlan.SendKeys(billCycle);
                    Thread.Sleep(2000);
                    //Wait for loading bar to be disappeared
                    WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                }
                #endregion

                #region Click on Calculate Premium
                //Assign Field name
                fieldName = "Policy : Click on Calculate Premium";
                var elementCalculatePremium = chromeDriver.FindElement(By.XPath(webElementsPremiumSummaryScreen.CalculatePremium));
                elementCalculatePremium.Click();
                Thread.Sleep(3000);

                //Wait for loading bar to be disappeared
                WaitWhileLoading(chromeDriver, webElements.LoadingBarDisappeared);
                #endregion

                //Check In Progress Div Error Status
                if (GetInProgressDivErrorStatus(chromeDriver, webElements.DivInprogress))
                {
                    errorRemarks = errorMsgs.FieldVerificationFailed.Replace("%FieldName%", fieldName).Replace("%Navigation%", nav_1 + "|" + nav_2 + "|" + nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    return screenAutomateStatus;
                }

                #region Check for submit errors
                var screenErrorPopup = chromeDriver.FindElements(By.XPath(webElementsPremiumSummaryScreen.PleaseConfirm));
                if (screenErrorPopup.Count > 0)
                {
                    GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
                    var btnYes = chromeDriver.FindElements(By.XPath(webElementsPremiumSummaryScreen.YesButton));
                    if (btnYes.Count > 0)
                    {
                        btnYes[0].Click();
                    }
                    Thread.Sleep(3000);
                    errorRemarks = errorMsgs.PageSubmitError.Replace("%Nav1%", nav_1).Replace("%Nav2%", nav_2).Replace("%Nav3%", nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                }
                #endregion

                //Navigation variables assignment
                nav_1 = "Premium Summary";
                nav_2 = string.Empty;
                nav_3 = string.Empty;

                #region General information Screen Found/NOT Found check
                generalInformationLabelText = string.Empty;
                iFramePremiumSummary = chromeDriver.FindElements(By.XPath(webElementsPremiumSummaryScreen.IFramePremiumSummary));
                if (iFramePremiumSummary != null && iFramePremiumSummary.Count > 0)
                {
                    chromeDriver.SwitchTo().Frame(iFramePremiumSummary[0]);
                    var elementGeneralInformationLabel = chromeDriver.FindElements(By.XPath(webElementsPremiumSummaryScreen.GeneralInformationLabel));
                    if (elementGeneralInformationLabel.Count > 0)
                    {
                        generalInformationLabelText = elementGeneralInformationLabel[0].Text;
                    }
                    chromeDriver.SwitchTo().DefaultContent();
                }

                if (!generalInformationLabelText.Contains("General Information"))
                {
                    errorRemarks = errorMsgs.PageNotFoundException.Replace("%Nav1%", nav_1).Replace("%Nav2%", nav_2).Replace("%Nav3%", nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    return screenAutomateStatus;
                }
                #endregion

                //Retrieve New policy Number
                var elementNewPolicyNumber = FindElementByMaxWait(chromeDriver, webElementsPremiumSummaryScreen.NewPolicy, 15);
                screenAutomateStatus.NewPolicyNumber = elementNewPolicyNumber.Text.Trim();

                #region Capping logic should be there
                double oldPremium = Convert.ToDouble(inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.PRIOR_POLICY_ANNUAL_PREM.Replace("$", "").Trim());
                CommonHelper commonHelper = new CommonHelper();
                bool cappingStatus = commonHelper.GetCappingStatus(folders.PayPlanPath, inputXmlData.DF_CORR_MASTER.AI_BOT_READY_DATA.PRIMARY_STATE, fullPayTransactionPremium, oldPremium);
                if (!cappingStatus)
                {
                    errorRemarks = "Failed for Premium Cap Issue";
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    return screenAutomateStatus;
                }
                #endregion

                //Navigation variables assignment
                nav_1 = "Policy Status : Dashboard";
                nav_2 = "Ready for Booking";
                nav_3 = string.Empty;

                //Click on Next
                btnNext = chromeDriver.FindElement(By.XPath(webElementsPremiumSummaryScreen.NextButton));
                btnNext.Click();
                Thread.Sleep(5000);

                //Switch to Dashboard
                iframeDashboard = FindElementByMaxWait(chromeDriver, webElements.IframeDashboard, 40);
                chromeDriver.SwitchTo().Frame(iframeDashboard);
                //Wait for status Ready for Booking
                status = WaitForTargetStatus(chromeDriver, webElements.DivStatusPolicy, nav_2, 300);
                if (!status)
                {
                    var elementDivStatusPolicy = chromeDriver.FindElements(By.XPath(webElements.DivStatusPolicy));
                    if (elementDivStatusPolicy.Count > 0)
                    {
                        nav_3 = "(PAS - " + elementDivStatusPolicy[0].GetAttribute("value").Trim() + ")";
                    }
                    errorRemarks = errorMsgs.PageNotFoundException.Replace("%Nav1%", nav_1).Replace("%Nav2%", nav_2).Replace("%Nav3%", nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    return screenAutomateStatus;
                }

                //Click on Book
                var btnBook = FindElementByMaxWait(chromeDriver, webElements.Book, 10);//chromeDriver.FindElement(By.XPath(webElements.Book));
                btnBook.Click();
                Thread.Sleep(3000);

                //Whole Page getting refreshed. Again iframe switch is needed
                //Switch to Dashboard
                iframeDashboard = FindElementByMaxWait(chromeDriver, webElements.IframeDashboard, 40);
                chromeDriver.SwitchTo().Frame(iframeDashboard);

                //Navigation variables assignment
                nav_1 = "Policy Status : Dashboard";
                nav_2 = "In Force";
                nav_3 = string.Empty;

                //Wait for status In Force
                status = WaitForTargetStatus(chromeDriver, webElements.DivStatusPolicy, nav_2, 300);
                if (!status)
                {
                    var elementDivStatusPolicy = chromeDriver.FindElements(By.XPath(webElements.DivStatusPolicy));
                    if (elementDivStatusPolicy.Count > 0)
                    {
                        nav_3 = "(PAS - " + elementDivStatusPolicy[0].GetAttribute("value").Trim() + ")";
                    }
                    errorRemarks = errorMsgs.PageNotFoundException.Replace("%Nav1%", nav_1).Replace("%Nav2%", nav_2).Replace("%Nav3%", nav_3);
                    screenAutomateStatus.Status = false;
                    screenAutomateStatus.ErrMsg = errorRemarks;
                    return screenAutomateStatus;
                }

                //Switch to Default Content
                chromeDriver.SwitchTo().DefaultContent();

                screenAutomateStatus.Status = true;
            }
            catch (Exception ex)
            {
                screenAutomateStatus.Status = false;
                screenAutomateStatus.ExpMsgTech = ex.Message;
                screenAutomateStatus.ErrMsg = "Technical Exception Occurred at Premium Summary Screen while automating";
                _logger.LogInformation("Technical Exception Occurred at Premium Summary Screen while automating");
                _logger.LogError(ex.Message);
                GenerateScreenShot(chromeDriver, folders.ErrorSnap, inputXmlData.DF_CORR_MASTER.POLICYNUMBER);
            }
            return screenAutomateStatus;
        }

        /// <summary>
        /// Find an element with max wait by
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="elementXPath"></param>
        /// <param name="timeoutInSeconds"></param>
        /// <returns></returns>
        private IWebElement FindElementByMaxWait(ChromeDriver driver, string elementXPath, int timeoutInSeconds)
        {
            if (timeoutInSeconds > 0)
            {
                //var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));
                //return wait.Until(drv => drv.FindElement(By.XPath(elementXPath)));
                Stopwatch sw = new Stopwatch();
                sw.Start();
                while (true)
                {
                    var webElements = driver.FindElements(By.XPath(elementXPath));
                    if (webElements.Count > 0)
                    {
                        return webElements[0];
                    }

                    if (sw.ElapsedMilliseconds > timeoutInSeconds * 1000)
                    {
                        break;
                    }
                    Thread.Sleep(1000);
                }
                sw.Stop();
            }
            return driver.FindElement(By.XPath(elementXPath));
        }

        /// <summary>
        /// Wait until an ajax request is completed
        /// </summary>
        /// <param name="javaScriptExecutor"></param>
        private void WaitForAjax(IJavaScriptExecutor javaScriptExecutor)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            while (true)
            {
                Thread.Sleep(100);
                var ajaxIsComplete = (bool)javaScriptExecutor.ExecuteScript("return jQuery.active == 0");
                if (ajaxIsComplete)
                    break;
                Thread.Sleep(100);

                if (sw.ElapsedMilliseconds > 60000)
                    break;  //breaking after 1 mins
            }
            sw.Stop();
        }

        /// <summary>
        /// Wait until a text to be appeared in an element.
        /// </summary>
        /// <param name="chromeDriver">webdriver object</param>
        /// <param name="elementXPath">In which element the target text to be searched.</param>
        /// <param name="targetTxt">wait for which text to be present</param>
        /// <param name="maxWaitByInSec">maximum number of wait in seconds</param>
        /// <returns>true/false</returns>
        private bool WaitForTargetStatus(ChromeDriver chromeDriver, string elementXPath, string targetTxt, int maxWaitByInSec)
        {
            bool status = false;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            while (true)
            {
                var webElement = chromeDriver.FindElements(By.XPath(elementXPath));
                if (webElement.Count > 0 && webElement[0].Text.Contains(targetTxt))
                {
                    status = true;
                    break;
                }

                if (sw.ElapsedMilliseconds > maxWaitByInSec * 1000)
                {
                    break;
                }

                Thread.Sleep(1000);
            }
            sw.Stop();
            Thread.Sleep(1000);
            return status;
        }

        /// <summary>
        /// Wait until loading bar disappeared
        /// </summary>
        /// <param name="chromeDriver"></param>
        /// <param name="loadingBarDisappearedXpath"></param>
        /// <returns></returns>
        private bool WaitWhileLoading(ChromeDriver chromeDriver, string loadingBarDisappearedXpath)
        {
            bool status = false;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            while (true)
            {
                Thread.Sleep(1000);
                int count = chromeDriver.FindElements(By.XPath(loadingBarDisappearedXpath)).Count();
                if (count > 0)
                {
                    status = true;
                    break;
                }
                Thread.Sleep(1000);

                if (sw.ElapsedMilliseconds > 60000)
                    break;  //breaking after 1 mins
            }
            sw.Stop();
            Thread.Sleep(1000);
            return status;
        }

        /// <summary>
        /// Get error status from In Progress status bar
        /// </summary>
        /// <param name="chromeDriver"></param>
        /// <param name="quoteStatusBar"></param>
        /// <returns></returns>
        private bool GetInProgressDivErrorStatus(ChromeDriver chromeDriver, string quoteStatusBar)
        {
            bool status = false;
            var quoteStatusBars = chromeDriver.FindElements(By.XPath(quoteStatusBar));
            if (quoteStatusBars.Count > 0)
            {
                string statusTxt = quoteStatusBars[0].GetAttribute("innerText");
                if (statusTxt == "Error")
                    return true;
                else
                    return false;
            }

            return status;
        }

        /// <summary>
        /// Generate error message
        /// </summary>
        /// <param name="nav_1"></param>
        /// <param name="nav_2"></param>
        /// <param name="nav_3"></param>
        /// <param name="expType"></param>
        /// <returns></returns>
        private string GenerateErrorMsg(string nav_1, string nav_2, string nav_3, string expType)
        {
            string errorRemarks = expType;
            try
            {
                errorRemarks = errorRemarks.Replace("%Nav1%", nav_1);
                errorRemarks = errorRemarks.Replace("%Nav2%", nav_2);
                errorRemarks = errorRemarks.Replace("%Nav3%", nav_3);
            }
            catch { }
            return errorRemarks;
        }

        /// <summary>
        /// Generate Screen Shot
        /// </summary>
        /// <param name="chromeDriver"></param>
        /// <param name="errorScreenshotPath"></param>
        /// <param name="fileNameWith"></param>
        private void GenerateScreenShot(ChromeDriver chromeDriver, string errorScreenshotPath, string fileNameWith)
        {
            try
            {
                string folderPath = errorScreenshotPath + "\\" + DateTime.Now.ToString("yyyy") + "\\" + DateTime.Now.ToString("MM") + "\\" + DateTime.Now.ToString("dd");
                bool exist = Directory.Exists(folderPath);
                if (!exist)
                {
                    Directory.CreateDirectory(folderPath);
                }
                string errorImagePath = folderPath + "\\" + fileNameWith + "_" + DateTime.Now.ToString("HHmmss_fff") + ".png";
                if (chromeDriver != null && chromeDriver.SessionId != null)
                {
                    var scrShot = chromeDriver.GetScreenshot();
                    scrShot.SaveAsFile(errorImagePath, ScreenshotImageFormat.Png);

                }
            }
            catch { }
        }
    }
}
