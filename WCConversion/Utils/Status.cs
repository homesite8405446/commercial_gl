﻿namespace WildfireMoratorium
{
    public static class Status
    {
        public const string Completed = "COMPLETED";
        public const string Exception = "EXCEPTION";
        public const string BusinessException = "BUSINESS EXCEPTION";
        public const string Success = "SUCCESS";
        public const string Failure = "FAILURE";
        public const string Partial = "PARTIAL";
        public const string Invalid = "INVALID";
    }
}
