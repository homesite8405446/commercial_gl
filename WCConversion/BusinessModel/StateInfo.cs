﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class StateInfo : IStateInfo
    {
        public string LongState { get; set; }
        public string StateType { get; set; }
        public bool IsFound { get; set; } = false;
    }
}
