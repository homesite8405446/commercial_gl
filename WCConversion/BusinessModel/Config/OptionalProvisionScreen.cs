﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class OptionalProvisionScreen
    {
        public string OptionalProvisionLabel { get; set; }
        public string Add { get; set; }
        public string FormDescriptionLabel { get; set; }
        public string FormDescription { get; set; }
        public string FormDescriptionTable { get; set; }
        public string FormDescriptionTableRow { get; set; }
        public string FormDescriptionTableRowFormDesc { get; set; }
        public string FormDescriptionTableRowFormNum { get; set; }
        public string DescOfOperations { get; set; }
        public string DescOfWork { get; set; }
        public string DecsLocationOfPremises { get; set; }
        public string LocationDescOfOps { get; set; }
        public string DecsOfOngoingOps { get; set; }
        public string DecsOfPremisesOps { get; set; }
        public string PersonOrgName { get; set; }
        public string ExcludedHazards { get; set; }
        public string CoveredHazard { get; set; }
        public string SpecificLocation { get; set; }
        public string NextButton { get; set; }
        public string PleaseConfirm { get; set; }
        public string YesButton { get; set; }
        public string HideElement { get; set; }
        public string DoneButton { get; set; }
    }
}
