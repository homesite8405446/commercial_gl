﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class ProducersScreen
    {
        public string ProducerLabel { get; set; }
        public string PartnerNumber { get; set; }
        public string QuoteNumber { get; set; }
        public string Channel { get; set; }
        public string Promotion { get; set; }
        public string AgencyNumber { get; set; }
        public string PartnerID { get; set; }
        public string ContactPartnerID { get; set; }
        public string ProducerID { get; set; }
        public string CdhID { get; set; }
        public string PriorPolicyNumber { get; set; }
        public string PriorAnnualPremium { get; set; }
        public string PriorInceptionYear { get; set; }
        public string Company { get; set; }
        public string NextButton { get; set; }
        public string PleaseConfirm { get; set; }
        public string YesButton { get; set; }
    }
}
