﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class CommercialUmbrellaScreen
    {
        public string ComUmbLabel { get; set; }
        public string State { get; set; }
        public string OccurrenceLimit { get; set; }
        public string SelfInsuredRetention { get; set; }
        public string NextButton { get; set; }
        public string PleaseConfirm { get; set; }
        public string YesButton { get; set; }
        public string OccurenceLimitTable { get; set; }
        public string OccurenceLimitTableRow { get; set; }
    }
}
