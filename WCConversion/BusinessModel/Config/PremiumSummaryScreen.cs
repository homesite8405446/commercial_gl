﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class PremiumSummaryScreen
    {
        public string PremiumSummaryLabel { get; set; }
        public string CalculatePremium { get; set; }
        public string IFramePremiumSummary { get; set; }
        public string GeneralInformationLabel { get; set; }
        public string NextButton { get; set; }
        public string Status { get; set; }
        public string ConvertToPolicy { get; set; }
        public string PolicyLabel { get; set; }
        public string FullPayTransactionPremium { get; set; }
        public string TotalPremium { get; set; }
        public string PaymentPlan { get; set; }
        public string NewPolicy { get; set; }
        public string PleaseConfirm { get; set; }
        public string YesButton { get; set; }
    }
}
