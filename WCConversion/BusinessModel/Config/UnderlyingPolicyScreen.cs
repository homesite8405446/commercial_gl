﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class UnderlyingPolicyScreen
    {
        public string UnderlyingPolicyLabel { get; set; }
        public string Add { get; set; }
        public string LineOfBusinessLabel { get; set; }
        public string LineOfBusiness { get; set; }
        public string LineOfBusinessTable { get; set; }
        public string LineOfBusinessTableRow { get; set; }
        public string AutomaticRadioButton { get; set; }
        public string ManualRadioButton { get; set; }
        public string PolicyNumber { get; set; }
        public string PolicyRadioButton { get; set; }
        public string QuoteRadioButton { get; set; }
        public string UnderWritingCompany { get; set; }
        public string EffectiveDate { get; set; }
        public string ExpirationDate { get; set; }
        public string PolicyStatus { get; set; }
        public string BopLiabilityLimit { get; set; }
        public string CaLiabilityLimit { get; set; }
        public string CpLiabilityLimit { get; set; }
        public string WcLiabilityLimit { get; set; }
        public string NextButton { get; set; }
        public string PleaseConfirm { get; set; }
        public string YesButton { get; set; }
        public string BopLiabilityLimitTable { get; set; }
        public string BopLiabilityLimitTableRow { get; set; }
        public string CaLiabilityLimitTable { get; set; }
        public string CaLiabilityLimitTableRow { get; set; }
        public string CpLiabilityLimitTable { get; set; }
        public string CpLiabilityLimitTableRow { get; set; }
        public string WcLiabilityLimitTable { get; set; }
        public string WcLiabilityLimitTableRow { get; set; }
        public string HideElement { get; set; }
        public string DoneButton { get; set; }
    }
}
