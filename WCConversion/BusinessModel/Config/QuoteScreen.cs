﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class QuoteScreen
    {
        public string HeadingLabel { get; set; }
        public string EffectiveDate { get; set; }
        public string BookRollPolicyCheckbox { get; set; }
        public string InsuredName { get; set; }
        public string InsuredType { get; set; }
        public string BusinessDescription { get; set; }
        public string FeinNumber { get; set; }
        public string BusinessDescriptionTable { get; set; }
        public string BusinessDescriptionTableRow { get; set; }
        public string BusinessStartYear { get; set; }
        public string InsuredCountry { get; set; }
        public string InsuredStreet { get; set; }
        public string InsuredStreet_2 { get; set; }
        public string InsuredCity { get; set; }
        public string InsuredState { get; set; }
        public string InsuredZip { get; set; }
        public string AddButton { get; set; }
        public string UnemploymentState { get; set; }
        public string UnemploymentNumber { get; set; }
        public string NextButton { get; set; }
        public string PleaseConfirm { get; set; }
        public string YesButton { get; set; }
    }
}
