﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class ErrorMsgs
    {
        public string FieldVerificationFailed { get; set; }
        public string UnexpectedException { get; set; }
        public string InputFolderInexistent { get; set; }
        public string ConfigFolderInexistent { get; set; }
        public string RatingError { get; set; }
        public string PageNotFoundException { get; set; }
        public string PageSubmitError { get; set; }
        public string CappingException { get; set; }
    }
}
