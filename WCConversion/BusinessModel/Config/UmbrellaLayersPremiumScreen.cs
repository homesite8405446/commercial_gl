﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class UmbrellaLayersPremiumScreen
    {
        public string UmbrellaLayersPremiumLabel { get; set; }
        public string LayersPremium { get; set; }
        public string NextButton { get; set; }
        public string PleaseConfirm { get; set; }
        public string YesButton { get; set; }
    }
}
