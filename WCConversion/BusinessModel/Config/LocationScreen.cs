﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class LocationScreen
    {
        public string AddButton { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string StateList { get; set; }
        public string Zip { get; set; }
        public string NextButton { get; set; }
        public string PleaseConfirm { get; set; }
        public string YesButton {get;set;}

    }
}
