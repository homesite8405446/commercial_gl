﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class EmailTemplates
    {
        public EmailInfo? StartEmail { get; set; }
        public EmailInfo? CompletionEmail { get; set; }
        public EmailInfo? HardStopEmail { get; set; }
        public EmailInfo? InputFileMissingEmail { get; set; }
        public EmailInfo? ShareDriveErrorEmail { get; set; }
        public EmailInfo? LoginFailedEmail { get; set; }
        public EmailInfo? ReportEmail { get; set; }
    }
}
