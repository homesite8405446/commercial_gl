﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class EmailConfig
    {
        public string? HostName { get; set; }
        public string? PortNo { get; set; }
        public string? ReportFromEmail { get; set; }
        public string? ReportToEmail { get; set; }
        public string? OpsTeamEmailAddress { get; set; }

    }
}
