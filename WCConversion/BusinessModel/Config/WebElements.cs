﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class WebElements
    {
        public string RdoBtnMajesco { get; set; }
        public string BtnMjescoSignIn { get; set; }
        public string InputUserID { get; set; }
        public string InputPassword { get; set; }
        public string ChkCookiePolicy { get; set; }
        public string BtnLogin { get; set; }
        public string ChildElement { get; set; }
        public string NextChildElement { get; set; }
        public string LoadingIdentifier { get; set; }
        public string WhatsNewPopupIframe { get; set; }
        public string WhatsNewSkipButton { get; set; }
        public string WhatsNewConfirmButton { get; set; }
        public string SelectDropDown { get; set; }
        public string WorkersComp { get; set; }
        public string LetsBegin { get; set; }
        public string LoadingBarDisappeared { get; set; }
        public string DivInprogress { get; set; }
        public string IconMajesco { get; set; }
        public string PopupCloseButton { get; set; }
        public string YesButton { get; set; }
        public string IframeDashboard { get; set; }
        public string DivStatusPolicy { get; set; }
        public string Book { get; set; }
        public QuoteScreen QuoteScreen { get; set; }
        public ProducersScreen ProducersScreen { get; set; }
        public CommercialUmbrellaScreen CommercialUmbrellaScreen { get; set; }
        public UnderlyingPolicyScreen UnderlyingPolicyScreen { get; set; }
        public OptionalProvisionScreen OptionalProvisionScreen { get; set; }
        public UmbrellaLayersPremiumScreen UmbrellaLayersPremiumScreen { get; set; }
        public PremiumSummaryScreen PremiumSummaryScreen { get; set; }
        public LocationScreen LocationScreen { get; set; }
    }
}
