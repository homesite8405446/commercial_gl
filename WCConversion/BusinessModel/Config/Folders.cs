﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class Folders
    {
        public string? InputPath { get; set; }
        public string? OutputPath { get; set; }
        public string? WCValidation { get; set; }
        public string? Processed { get; set; }
        public string? Unprocessed { get; set; }
        public string? ErrorSnap { get; set; }
        public string? TempFolder { get; set; }
        public string? ArchiveFolderPath { get; set; }
        public string? StateConversionFullFilePath { get; set; }
        public string? PayPlanPath { get; set; }
    }
}
