﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class AppEnvironment
    {
        public string URL { get; set; }
        public string UserID { get; set; }
        public string Pwd { get; set; }
        public string LoginWindowTitle { get; set; }
    }
}
