﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class PayPlan
    {
        public string State { get; set; } = string.Empty;
        public double MinAdjustedPayPlan { get; set; }
        public double MaxAdjustedPayPlan { get; set; }
    }
}
