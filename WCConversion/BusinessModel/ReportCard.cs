﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class ReportCard : IReportCard
    {
		public string ProcessName { get; set; }
		public string TotalFilesCount { get; set; }
		public string SuccessFilesCount { get; set; }
		public string FailureFilesCount { get; set; }
	}
}
