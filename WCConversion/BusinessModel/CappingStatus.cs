﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class CappingStatus : ICappingStatus
    {
        public bool LowerBoundary { get; set; }
        public bool UpperBoundary { get; set; }
        public bool Status { get; set; }
    }
}
