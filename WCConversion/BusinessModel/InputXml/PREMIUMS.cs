﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class PREMIUMS
    {
        public string POLICY_NUMBER { get; set; }
        public string LAYER_NUM { get; set; }
        public string LAYER_PREMIUM_TYPE { get; set; }
        public string LAYER_PREMIUM { get; set; }
    }
}
