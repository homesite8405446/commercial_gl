﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class WC_DATA
    {
        public string BUSINESS_FEIN_PRESENT { get; set; }
        public string BUSINESS_FEIN { get; set; }
    }
}
