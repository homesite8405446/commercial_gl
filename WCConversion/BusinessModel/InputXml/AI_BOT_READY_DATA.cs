﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class AI_BOT_READY_DATA
    {
        public string POLICY_TYPE_DESC { get; set; }
        public string COMPANY { get; set; }
        public string EFFECTIVE_DATE { get; set; }
        public string EXPIRE_DATE { get; set; }
        public string BOOKROLLPOLICY { get; set; }
        public string BILL_CYCLE { get; set; }
        public string INDUSTRY { get; set; }
        public string BUSINESS_NAME { get; set; }
        public string BUSINESS_FORM { get; set; }
        public string BUSINESS_DESC { get; set; }
        public string BUSINESS_FEIN { get; set; }
        public string BUSINESS_COUNTRY { get; set; }
        public string BUSINESS_ADDR_1 { get; set; }
        public string BUSINESS_ADDR_2 { get; set; }
        public string BUSINESS_ADDR_CITY { get; set; }
        public string BUSINESS_ADDR_STATE { get; set; }
        public string BUSINESS_ADDR_ZIP { get; set; }
        public string WC_UNEMP_STATE { get; set; }
        public string WC_STATE_UNEMP_ID { get; set; }
        public string EMAIL { get; set; }
        public string PHONE { get; set; }
        public string CONTACT_NAME { get; set; }
        public string POSTAL_CODE { get; set; }
        public string BUSINESS_INCEP_YR { get; set; }
        public string DIST_PARTNER_ID { get; set; }
        public string CHANNEL { get; set; }
        public string PROMOTION { get; set; }
        public string AGENCY_NUM { get; set; }
        public string AGENCY_PRODUCER_ID { get; set; }
        public string PRODUCER_ID { get; set; }
        public string PARTNER_ID { get; set; }
        public string CONTACT_PARTNER_ID { get; set; }
        public string AMFAM_CDH_ID { get; set; }
        public string PRIOR_POLICY_NO { get; set; }
        public string PRIOR_POLICY_ANNUAL_PREM { get; set; }
        public string PRIOR_POLICY_INCEPT_YR { get; set; }
        public string PRIOR_UW_CO { get; set; }
        public string PRIOR_POLICY_EFF_DT { get; set; }
        public string PRIOR_POLICY_EXP_DT { get; set; }
        public string POLICY_START_DATE { get; set; }
        public string MAIL_ADDR1 { get; set; }
        public string MAIL_ADDR2 { get; set; }
        public string MAIL_ADDR_CITY { get; set; }
        public string MAIL_ADDR_ST { get; set; }
        public string MAIL_ADDR_ZP { get; set; }
        public string PRIMARY_STATE { get; set; }
        public string UMB_EACH_OCCURRENCE_LIMIT { get; set; }
        public string SELFINSURED_RETENTION { get; set; }
        public string EXCEPTION { get; set; }
        public List<POLICY> OTHER_POLICIES { get; set; }
        public List<PREMIUMS> LAYER_PREMIUMS { get; set; }
        public List<FORM> FORMS { get; set; }
        public WC_DATA WC_DATA { get; set; }
        //Need sample child nodes
        public ADDL_INSUREDS ADDL_INSUREDS { get; set; }

        //Need sample child nodes
        public ADDL_INTERESTS ADDL_INTERESTS { get; set; }
       
        public List<LOCATION> LOCATIONS { get; set; }
    }
}
