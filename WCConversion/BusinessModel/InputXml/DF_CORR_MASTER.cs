﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class DF_CORR_MASTER
    {
        public string TRANSACTIONGUID { get; set; }
        public string LANGUAGE { get; set; }
        public string REQUESTDATE { get; set; }
        public string TEMPLATENAME { get; set; }
        public string POLICYTYPE { get; set; }
        public string POLICYSOURCE { get; set; }
        public string POLICYNUMBER { get; set; }
        public string EFFECTIVEDATE { get; set; }
        public string POLICY_TYPE_DESC { get; set; }
        public string DELIVERYMETHOD { get; set; }
        public string COMP_ADDR1 { get; set; }
        public string COMP_CITY { get; set; }
        public string COMP_STATE { get; set; }
        public string COMP_ZIPCODE { get; set; }
        public string COMP_PHONE { get; set; }
        public string PARTNERCODE { get; set; }
        public string BRANDCODE { get; set; }
        public string MARKETPROG { get; set; }
        public string MAIL_NAME1 { get; set; }
        public string MAIL_ADDR1 { get; set; }
        public string MAIL_ADDR2 { get; set; }
        public string MAIL_CITY { get; set; }
        public string MAIL_STATE { get; set; }
        public string MAIL_ZIPCODE { get; set; }
        public AI_BOT_READY_DATA AI_BOT_READY_DATA { get; set; }
    }
}
