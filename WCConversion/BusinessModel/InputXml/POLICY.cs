﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class POLICY
    {
        public string POLICY_NUMBER { get; set; }
        public string POLICY_STATUS { get; set; }
        public string EFFECTIVE_DATE { get; set; }
        public string EXPIRE_DATE { get; set; }
        public string POLICY_LOB { get; set; }
        public string POLICY_DESC { get; set; }
        public string POLICY_RATE_STATE { get; set; }
        public string POLICY_SETUP_TYPE { get; set; }
        public string POLICY_TYPE { get; set; }
        public string POLICY_UW_COMPANY { get; set; }
        public string POLICY_TOTAL_PREM { get; set; }
        public string BO_OCCUR_LIMIT { get; set; }
        public string BO_AGGR_LIMIT { get; set; }
        public string BO_LIAB_LIMIT { get; set; }
        public string BO_PROD_PREM { get; set; }
        public string BO_PI_PREM { get; set; }
        public string AU_OCCUR_LIMIT { get; set; }
        public string AU_AGGR_LIMIT { get; set; }
        public string AU_LIAB_LIMIT { get; set; }
        public string AU_PROD_PREM { get; set; }
        public string AU_PI_PREM { get; set; }
        public string CP_OCCUR_LIMIT { get; set; }
        public string CP_AGGR_LIMIT { get; set; }
        public string CP_LIAB_LIMIT { get; set; }
        public string CP_PROD_PREM { get; set; }
        public string CP_PI_PREM { get; set; }
        public string WC_EMPL_LIAB_LIMIT { get; set; }
    }
}
