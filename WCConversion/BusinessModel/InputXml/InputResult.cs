﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace WCConversion
{
    [XmlRootAttribute("RESULTS")]
    public class InputResult
    {
        public DF_CORR_MASTER DF_CORR_MASTER { get; set; }
    }
}
