﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class LOCATION
    {
       public string WC_CLASS_POLICY { get; set; }
        public string BUSINESS_NAME { get; set; }
        public string BUSINESS_ENTITY { get; set; }
        public string LOCATION_NUMBER { get; set; }
        public string BUSINESS_ADDR_1 { get; set; }
        public string BUSINESS_ADDR_2 { get; set; }
        public string BUSINESS_ADDR_CITY { get; set; }
        public string BUSINESS_ADDR_STATE { get; set; }
        public string BUSINESS_ADDR_ZIP { get; set; }
        public string BUSINESS_ADDR_CNTY { get; set; }
        public string BUSINESS_ADDR_ST { get; set; }

    }
}
