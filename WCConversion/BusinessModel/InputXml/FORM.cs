﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class FORM
    {
        public string POLICY_NUMBER { get; set; }
        public string FORM_OCC { get; set; }
        public string FORM_CODE { get; set; }
        public string FORM_NAME { get; set; }
        public string FORM_PROMPT1 { get; set; }
        public string FORM_PROMPT2 { get; set; }
    }
}
