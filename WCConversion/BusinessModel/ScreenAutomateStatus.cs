﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class ScreenAutomateStatus: IScreenAutomateStatus
    {
        public bool Status { get; set; } = false;
        public string ErrMsg { get; set; } = string.Empty;
        public string ExpMsgTech { get; set; } = string.Empty;
        public string QuoteNumber { get; set; } = string.Empty;
        public string TotalPremium { get; set; } = string.Empty;
        public string NewPolicyNumber { get; set; } = string.Empty;
        public bool StatusPartial { get; set; } = false;
    }
}
