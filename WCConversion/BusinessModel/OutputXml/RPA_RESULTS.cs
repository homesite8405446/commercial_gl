﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace WCConversion
{
    [XmlRoot("RPA_RESULTS")]
    public class RPA_RESULTS
    {
        public string PRIORPOLICYNUMBER { get; set; } = string.Empty;
        public string TRANSACTIONGUID { get; set; } = string.Empty;
        public string QUOTENUM { get; set; } = string.Empty;
        public string POLICYNUM { get; set; } = string.Empty;
        public string STATUS { get; set; } = string.Empty;
        public string ERRMSG { get; set; } = string.Empty;
        public string TOTAL_PREMIUM { get; set; } = string.Empty;
        public string POLICYTYPE { get; set; } = string.Empty;
        public string POLICYSOURCE { get; set; } = string.Empty;
        public string XML_NAME { get; set; } = string.Empty;
    }
}
