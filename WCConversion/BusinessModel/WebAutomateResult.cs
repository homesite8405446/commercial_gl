﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public class WebAutomateResult : IWebAutomateResult
    {
        public bool LoginStatus { get; set; }
        public string ErrorMessage { get; set; }
        public List<InputResult> ProcessedXml { get; set; } = new List<InputResult>();
        public List<InputResult> UnProcessedXml { get; set; } = new List<InputResult>();
        public List<InputResult> InvalidXml { get; set; } = new List<InputResult>();
    }
}
