﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Core;
using System.ComponentModel.Design;
using System.Security.Principal;

namespace WCConversion
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile($"appsettings.json", optional: false);
            IConfiguration config = builder.Build();

            var logFile = config["LogFile"].Replace("{YY}", DateTime.Now.ToString("yyyy")).Replace("{MM}", DateTime.Now.ToString("MM")).Replace("{CurrDate}", DateTime.Now.ToString("MM_dd_yyyy"));
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Console()
                .WriteTo.File(logFile)
                .CreateLogger();

            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            var serviceProvider = serviceCollection.BuildServiceProvider();
            var logger = serviceProvider.GetService<ILogger<Program>>();

            var serverName = Environment.MachineName;
            var env = config["Environment"];
            var logConnectionStringPath = config.GetConnectionString("RPALogConnectionStringPath");
            var appEnvironment = config.GetSection("AppEnvironments:" + env).Get<AppEnvironment>();
            var emailConfig = config.GetSection("EmailConfig").Get<EmailConfig>();
            var emailTemplates = config.GetSection("EmailTemplates").Get<EmailTemplates>();
            var folders = config.GetSection("Folders").Get<Folders>();
            var webElements = config.GetSection("WebElements").Get<WebElements>();
            var errorMsgs = config.GetSection("ErrorMsgs").Get<ErrorMsgs>();
            var oktaLogin = Convert.ToBoolean(config["OktaLogin"]);
            var underlyingAutomaticSelection = Convert.ToBoolean(config["UnderlyingAutomaticSelection"]);
            var botRetryCount = Convert.ToInt32(config["BotRetryCount"]);
            var inputFile = config["InputFile"];

            CommonHelper commonHelper = new CommonHelper();
            WebAutomationHelper webAutomationHelper = serviceProvider.GetService<WebAutomationHelper>();
            string logConnectionString = commonHelper.GetSecretKey(logConnectionStringPath);
            LogService logService = new LogService(logConnectionString); // Used for DB log

            logger.LogInformation("WC Conversion BOT START");
            try
            {
                logger.LogInformation("Checking if Input Path is accessible or not");
                if (commonHelper.IsDirectoryWritable(folders.InputPath))
                {
                    logger.LogInformation("Checking if any input file is available or not");
                    if (commonHelper.GetFileStatus(folders.InputPath, inputFile) || commonHelper.GetFileStatus(folders.TempFolder, inputFile))
                    {
                        string strStartDateTime = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");
                        EmailService.SendStartEmail(emailConfig, emailTemplates.StartEmail, serverName, strStartDateTime);

                        logger.LogInformation("Retrieving all input xml file list");
                        var inputList = commonHelper.GetFileList(folders.InputPath, inputFile);

                        logger.LogInformation("Automating PAS V11 Website START");
                        IWebAutomateResult webAutomateResult = webAutomationHelper.AutomateWebsite(folders, inputList, appEnvironment, webElements, errorMsgs, oktaLogin, botRetryCount, underlyingAutomaticSelection);
                        logger.LogInformation("Automating PAS V11 Website END");
                        if (webAutomateResult.LoginStatus)
                        {
                            logger.LogInformation("Generating Consolidated csv file based on output response");
                            var csvFullFileName = commonHelper.GenerateOutputCSV(folders.WCValidation);
                            List<string> attachmentList = new List<string> { csvFullFileName }; 
                            logger.LogInformation("Sending Completion Email");
                            EmailService.SendCompletionEmail(emailConfig, emailTemplates.CompletionEmail, serverName, strStartDateTime, DateTime.Now.ToString("MM/dd/yyyy hh:mm tt"), attachmentList);

                        }
                        else {
                            logger.LogInformation("Sending Login Failed Email");
                            EmailService.SendLoginFailedEmail(emailConfig, emailTemplates.LoginFailedEmail, serverName, webAutomateResult.ErrorMessage);
                        }
                    }
                    else {
                        logger.LogInformation("Sending Input File Missing Email");
                        EmailService.SendInputFileMissingEmail(emailConfig, emailTemplates.InputFileMissingEmail, serverName, "There is no input files to be processed.", folders.InputPath);
                    }
                }
                else
                {
                    logger.LogInformation("Sending Shared Drive Error Email");
                    EmailService.SendShareDriveErrorEmail(emailConfig, emailTemplates.ShareDriveErrorEmail, serverName, folders.InputPath);
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                EmailService.SendHardStopEmail(emailConfig, emailTemplates.HardStopEmail, serverName, DateTime.Now.ToString("MM/dd/yyyy hh:mm tt"), ex.Message, ex.StackTrace);
            }
            finally { }
            logger.LogInformation("WC Conversion BOT END");
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging(configure => configure.AddSerilog())
            .AddTransient<WebAutomationHelper>();
        }
    }
}