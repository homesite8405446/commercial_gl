﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public interface IScreenAutomateStatus
    {
        bool Status { get; set; }
        string ErrMsg { get; set; }
        string ExpMsgTech { get; set; }
        string QuoteNumber { get; set; }
        string TotalPremium { get; set; }
        string NewPolicyNumber { get; set; }
        bool StatusPartial { get; set; }
    }
}
