﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public interface IStateInfo
    {
        string LongState { get; set; }
        string StateType { get; set; }
        bool IsFound { get; set; }
    }
}
