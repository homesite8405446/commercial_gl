﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public interface ICappingStatus
    {
        bool LowerBoundary { get; set; }
        bool UpperBoundary { get; set; }
        bool Status { get; set; }
    }
}
