﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public interface IWebAutomateResult
    {
        bool LoginStatus { get; set; }
        string ErrorMessage { get; set; }
        List<InputResult> ProcessedXml { get; set; }
        List<InputResult> UnProcessedXml { get; set; }
        List<InputResult> InvalidXml { get; set; }
    }
}
