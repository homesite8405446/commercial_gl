﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCConversion
{
    public interface IReportCard
	{
		string ProcessName { get; set; }
		string TotalFilesCount { get; set; }
		string SuccessFilesCount { get; set; }
		string FailureFilesCount { get; set; }
	}
}
